<?php

class MyMpdf {

	private $pdf;

	public function newPDF ($mode='',$format='A4',$default_font_size=0,$default_font='',$mgl=15,$mgr=15,$mgt=16,$mgb=16,$mgh=9,$mgf=9, $orientation='P') {
		$this->pdf = new mPDF($mode, $format, $default_font_size, $default_font, $mgl, $mgr, $mgt, $mgb, $mgh, $mgf, $orientation);
	}

	public function write ($html) {
		$this->pdf->WriteHTML($html);
	}

	public function output ($filename=null, $dest='I') {
		return Response::make($this->pdf->Output($filename, $dest), 200, array(
			'content-type' => 'application/pdf'
		));
	}
}