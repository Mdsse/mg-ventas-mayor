<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

App::missing(function($exception)
{
    return Response::view('errores.missing', array(), 404);
});

Route::group(array('before' => 'auth'), function()
{
	Route::get('/', function ()
	{
		return Redirect::to('/productos/index');
	});

	Route::controller('usuarios', 'UsuarioController');
	Route::controller('clientes', 'ClienteController');
	Route::controller('productos', 'ProductoController');
	Route::controller('pedidos', 'PedidoController');
});

Route::get('/login', function ()
{
	return View::make('login');
})
->before('guest');

Route::post('/login', function ()
{
	$user = array(
        'username' => Input::get('username'),
        'password' => Input::get('password')
    );

    if (Auth::attempt($user)) {
        $msg['flag'] = 1;
        return Response::json($msg);
    }
    
    // authentication failure! lets go back to the login page
    $msg['msg'] = 'Usuario y/o contraseña inválido';
    $msg['flag'] = 0;
	return Response::json($msg);
});

Route::get('/logout', function ()
{
	Auth::logout();
	return Redirect::to('/login');
})
->before('auth');