<?php

class ClienteController extends BaseController {

	public function getIndex()
	{
		return View::make('clientes.index');
	}

	public function getModificar($id)
	{
		if (!preg_match('/^[0-9]+$/', $id))
			App::abort(404);

		return View::make('clientes.modificar');
	}

	public function postCargarTodos()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');

			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para ver datos de los clientes';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$msg['clients'] = Cliente::select('id', 'tipo_cliente', 'documento_identidad', 'nombre_cliente', 'email')->get();

			foreach($msg['clients'] as $key=>$value)
				$msg['clients'][$key]['tlf'] = Cliente::find($msg['clients'][$key]['id'])->telefonos()->pluck('tlf');

			$msg['flag'] = 1;
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron cargar los datos de los clientes\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postBuscar()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para buscar clientes';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$busqueda = Input::all()[0];
			$clients = Cliente::join('telefonos_clientes', 'clientes.id', '=', 'id_cliente')
				                ->select('clientes.id', 'tipo_cliente', 'documento_identidad', 'nombre_cliente', 'tlf', 'email')
				                ->whereRaw('tipo_cliente LIKE \'%'.$busqueda.'%\'
											OR documento_identidad LIKE \'%'.$busqueda.'%\'
											OR nombre_cliente LIKE \'%'.$busqueda.'%\'
											OR tlf LIKE \'%'.$busqueda.'%\'
											OR email LIKE \'%'.$busqueda.'%\'')
				                ->groupBy('id_cliente')
				                ->get();

			if(count($clients) === 0) {
				$msg['msg'] = 'No se pudieron encontrar resultados según lo indicado en la búsqueda';
				$msg['flag'] = -1;
			} else {
				$msg['clients'] = $clients;
				$msg['flag'] = 1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron encontrar resultados según lo indicado en la búsqueda\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postCargarCliente()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para cargar los datos de otros clientes';
				$msg['flag'] = -2;
				return Response::json($msg);
			}

			$clientData = Cliente::select('id', 'tipo_cliente', 'documento_identidad', 'nombre_cliente',
				                		 'estado_ubicacion', 'ciudad_ubicacion', 'direccion', 'email')
				                ->where('id', '=', Input::get('id'))
				                ->get();

			if($clientData) {
				$msg['clientData'] = $clientData[0];
				$msg['clientData']['tlf'] = Cliente::find($msg['clientData']['id'])->telefonos()->lists('tlf');
				$tlfs = [];

				foreach($msg['clientData']['tlf'] as $key=>$value)
					$tlfs[$key] = explode('-', $value);

				$msg['clientData']['tlf'] = $tlfs;

				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'No se pudieron encontrar los datos del cliente indicado';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron encontrar los datos del cliente indicado\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postRegistrar()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para agregar un cliente';
				$msg['flag'] = -3;
				return Response::json($msg);
			}
			$data = Input::all();

			if(Cliente::where('documento_identidad', '=', $data['documento_identidad'])->pluck('documento_identidad')) {
				$msg['msg'] = 'El documento de identidad indicada ya existe';
				$msg['flag'] = -2;
				return Response::json($msg);
			}

			$data['usuario_registro'] = $data['usuario_ua'] = Auth::id();
			$tlf = '';

			foreach ($data['tlf'] as $key=>$phones){
				foreach ($phones as $value)
	            	$tlf .= $value . '-';
	            $dataPhone[$key]['tlf'] = substr_replace($tlf, '', strlen($tlf) - 1);
	            $tlf = '';
			}
			unset($data['tlf']);

			if($idClient = Cliente::insertGetId($data)) {
				$flag = true;

				foreach ($dataPhone as $key=>$value)
					if( !TelefonoCliente::insert(array('tlf' => $dataPhone[$key]['tlf'], 'id_cliente' => $idClient)) )
						$flag = false;

				$msg['msg'] = 'Cliente agregado exitosamente\n';

				if($flag) {
					$msg['flag'] = 1;

				} else {
					$msg['msg'] = 'Pero alguno(s) de lo(s) teléfono(s) indicado(s) no se pudo agregar, por favor verifique';
					$msg['flag'] = -1;
				}

			} else {
				$msg['msg'] = 'Error en la base de datos, no se pudo agregar el cliente';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo agregar el cliente\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postActualizar()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para actualizar un cliente';
				$msg['flag'] = -4;
				return Response::json($msg);
			}
			$data = Input::all();
			$result = Cliente::select('id', 'documento_identidad')
							->where('documento_identidad', '=', $data['documento_identidad'])
							->first();

			if(!empty($result['documento_identidad']) && $result['id'] !== $data['id']) {
				$msg['msg'] = 'El documento de identidad indicado ya existe';
				$msg['flag'] = -3;
				return Response::json($msg);
			}

			$data['usuario_ua'] = Auth::id();
			$tlf = '';
			$tlfs = [];

			foreach ($data['tlf'] as $key=>$phones){
				foreach ($phones as $value)
	            	$tlf .= $value . '-';
	            $tlfs[$key] = substr_replace($tlf, '', strlen($tlf) - 1);
	            $tlf = '';
			}
			unset($data['tlf']);

			if(!TelefonoCliente::where('id_cliente', '=', $data['id'])->delete()){
				$msg['msg'] = 'Error en la base de datos actualizando los números telefónicos del cliente';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$flag = true;

			foreach ($tlfs as $key=>$value)
				if( !TelefonoCliente::insert(array('tlf' => $tlfs[$key], 'id_cliente' => $data['id'])) )
					$flag = false;

			if(Cliente::where('id', '=', $data['id'])->update($data)) {
				$msg['msg'] = 'Cliente actualizado exitosamente\n';

				if($flag) {
					$msg['flag'] = 1;

				} else {
					$msg['msg'] = 'Pero alguno(s) de lo(s) teléfono(s) indicado(s) no se pudo actualizar, por favor verifique';
					$msg['flag'] = -1;
				}

			} else {
				$flag = true;
				$result = Cliente::where('id', '=', $data['id'])->first();
				unset($result['usuario_registro']);
				unset($result['fecha_registro']);

				foreach ($data as $key => $value)
					if($result[0][$key] === $data[$key])
						$flag = false;

				if($flag)
					$msg['msg'] = 'El cliente ya está actualizado';
				else
					$msg['msg'] = 'Error en la base de datos, no se pudo actualizar el cliente';

				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo actualizar el cliente\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postEliminar()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para eliminar clientes';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$clientIds = Input::all();
			$idCount = count($clientIds);
			$delete = true;

			foreach ($clientIds as $id)
				if(!Cliente::where('id', '=', $id)->delete())
					$delete = false;
			

			if($delete) {
				$msg['msg'] = ($idCount === 1) ? 'Cliente eliminado exitosamente' : 'Clientes indicados eliminados exitosamente';
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'Error con la base de datos, ';
		        $msg['msg'] .= ($idCount === 1) ? 'no se pudo eliminar el cliente'
		        					: 'no se pudieron eliminar alguno(s) o todos los clientes indicados, por favor verifique';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudo eliminar ningún cliente\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}
}