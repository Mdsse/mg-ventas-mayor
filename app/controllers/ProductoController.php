<?php

class ProductoController extends BaseController {

	public function getIndex()
	{
		return View::make('productos.index');
	}

	public function getModificar($id)
	{
		if (!preg_match('/^[0-9]+$/', $id))
			App::abort(404);

		return View::make('productos.modificar');
	}

	public function postCargarTodos()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');

			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para ver datos de los productos';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$msg['products'] = Producto::select('id', 'codigo', 'cantidad', 'precio1', 'precio2')
										->orderBy('codigo')
										->get();
			$msg['flag'] = 1;
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron cargar los datos de los productos\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postBuscar()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para buscar productos';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$busqueda = Input::all()[0];
			$products = Producto::select('id', 'codigo', 'cantidad', 'precio1', 'precio2')
								->whereRaw('codigo LIKE \'%'.$busqueda.'%\'
											OR cantidad LIKE \'%'.$busqueda.'%\'
											OR precio1 LIKE \'%'.$busqueda.'%\'
											OR precio2 LIKE \'%'.$busqueda.'%\'')
								->orderBy('codigo')
								->get();
			if(count($products) === 0) {
				$msg['msg'] = 'No se pudieron encontrar resultados según lo indicado en la búsqueda';
				$msg['flag'] = -1;
			} else {
				$msg['products'] = $products;
				$msg['flag'] = 1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron encontrar resultados según lo indicado en la búsqueda\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postCargarProducto()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para cargar los datos de otros productos';
				$msg['flag'] = -2;
				return Response::json($msg);
			}

			$productData = Producto::select('id', 'codigo', 'imagen', 'descripcion', 'cantidad', 'punto_reorden',
											'bulto', 'costo', 'precio1', 'precio2', 'origen')
									->where('id', '=', Input::get('id'))
									->get()[0];

			if($productData) {
				$msg['productData'] = $productData;
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'No se pudieron encontrar los datos del producto indicado';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron encontrar los datos del producto indicado\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postRegistrar()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para agregar un producto';
				$msg['flag'] = -5;
				return Response::json($msg);
			}
			$data = Input::all();

			if(Producto::where('codigo', '=', $data['codigo'])->pluck('codigo')) {
				$msg['msg'] = 'El código indicado ya existe';
				$msg['flag'] = -4;
				return Response::json($msg);
			}

			if($data['origen'] !== 'Importado' && $data['origen'] !== 'Nacional') {
				$msg['msg'] = 'El valor de origen del producto debe ser \'Importado\' o \'Nacional\'';
				$msg['flag'] = -3;
				return Response::json($msg);
			}
			$data['usuario_registro'] = $data['usuario_ua'] = Auth::id();
			$config = json_decode( file_get_contents(URL::to('/configuracion.json')) );
			$cost = $data['costo'];

			if($data['origen'] === 'Importado') {
				$cost *= $config->factorCambio * $config->porcentajeGastosNacionalizacion;
			}
			$cost *= $config->porcentajeGastosAdministrativos;
			$data['precio1'] = ceil($cost / $config->porcentajePrecio1);
			$data['precio2'] = ceil($cost / $config->porcentajePrecio2);

			if($id = Producto::insertGetId($data)) {
				$msg['msg'] = 'Producto agregado exitosamente';
				$msg['id'] = $id;
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'Error en la base de datos, no se pudo agregar el producto';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo agregar el producto\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postGuardarImagen($id, $update) {
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para agregar un producto';
				$msg['flag'] = -3;
				return Response::json($msg);
			}
			$msg['flag'] = 1;
			
			if(Input::hasFile('imagen')) {
				$img = Input::file('imagen');
			
				if(!$img->isValid()) {
					$msg['msg'] = 'El archivo colocado como imagen no es válido';
					$msg['flag'] = -2;
					return Response::json($msg);	
				}

				if($update)
					File::delete(mb_convert_encoding(Producto::where('id', '=', $id)->pluck('imagen'), 'ISO-8859-1'));

				$aux = $filename = mb_convert_encoding ($img->getClientOriginalName(), 'ISO-8859-1');
				$imgPath = public_path() . '/img/productos/';
				$cont = 1;

				while(File::exists($imgPath . $aux)) {
					$extension = '.' . File::extension($imgPath . $aux);
					$aux = str_replace($extension, '-' . $cont++ . $extension, $filename);
				}
				$filename = $aux;
				$img->move($imgPath, $filename);
				$data['imagen'] = str_replace('\\', '/', $imgPath . utf8_encode($filename));

				if(!Producto::where('id', '=', $id)->update($data)) {
					$msg['msg'] = 'Error en el servidor, no se pudo agregar la imagen del producto';
					$msg['flag'] = -1;
				}
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo agregar la imagen del producto\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postActualizar()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para actualizar un producto';
				$msg['flag'] = -5;
				return Response::json($msg);
			}
			$data = Input::all();
			$result = Producto::select('id', 'codigo')
								->where('codigo', '=', $data['codigo'])
								->first();

			if(!empty($result['codigo']) && $result['id'] !== $data['id']) {
				$msg['msg'] = 'El código indicado ya existe';
				$msg['flag'] = -4;
				return Response::json($msg);
			}

			if($data['origen'] !== 'Importado' && $data['origen'] !== 'Nacional') {
				$msg['msg'] = 'El valor de origen del producto debe ser \'Importado\' o \'Nacional\'';
				$msg['flag'] = -3;
				return Response::json($msg);
			}
			$data['usuario_ua'] = Auth::id();
			$config = json_decode( file_get_contents(URL::to('/configuracion.json')) );
			$cost = $data['costo'];

			if($data['origen'] === 'Importado') {
				$cost *= $config->factorCambio * $config->porcentajeGastosNacionalizacion;
			}
			$cost *= $config->porcentajeGastosAdministrativos;
			$data['precio1'] = ceil($cost / $config->porcentajePrecio1);
			$data['precio2'] = ceil($cost / $config->porcentajePrecio2);

			if(Producto::where('id', '=', $data['id'])->update($data)) {
				$msg['msg'] = 'Producto actualizado exitosamente';
				$msg['precio1'] = $data['precio1'];
				$msg['precio2'] = $data['precio2'];
				$msg['flag'] = 1;

			} else {
				$flag = true;
				$result = Producto::where('id', '=', $data['id'])->first();
				unset($data['bulto']);
				unset($data['imagen']);
				unset($result['bulto']);
				unset($result['imagen']);
				unset($result['usuario_registro']);
				unset($result['fecha_registro']);

				foreach ($data as $key => $value)
					if($result[0][$key] === $data[$key])
						$flag = false;

				if($flag) {
					$msg['precio1'] = $data['precio1'];
					$msg['precio2'] = $data['precio2'];
					$msg['msg'] = 'El producto ya está actualizado';
					$msg['flag'] = 1;

				} else {
					$msg['msg'] = 'Error en la base de datos, no se pudo actualizar el producto';
					$msg['flag'] = -1;
				}
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo actualizar el producto\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postEliminar()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para eliminar productos';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$productIds = Input::all();
			$idCount = count($productIds);
			$images = Producto::whereIn('id', $productIds)->lists('imagen');
			$delete = true;

			foreach($images as $key=>$image)
				$images[$key] = mb_convert_encoding($image, 'ISO-8859-1');

			if(!Producto::whereIn('id', $productIds)->delete())
				$delete = false;

			if($delete) {
				File::delete($images);
				$msg['msg'] = ($idCount === 1) ? 'Producto eliminado exitosamente' : 'Productos indicados eliminados exitosamente';
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'Error con la base de datos, ';
		        $msg['msg'] .= ($idCount === 1) ? 'no se pudo eliminar el producto'
		        					: 'no se pudieron eliminar los productos indicados';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudo eliminar ningún producto\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}
}