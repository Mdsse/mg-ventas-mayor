<?php

class UsuarioController extends BaseController {

	public function getIndex()
	{
		return View::make('usuarios.index');
	}

	public function getConfiguracion()
	{
		if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador')
			App::abort(404);

		return View::make('usuarios.configuracion');
	}

	public function getPerfil()
	{
		return View::make('usuarios.modificar');
	}

	public function getCambiarClave()
	{
		return View::make('usuarios.cambiar-clave');
	}

	public function getModificar($id)
	{
		if (!preg_match('/^[0-9]+$/', $id))
			App::abort(404);

		return View::make('usuarios.modificar');
	}

	public function postConfiguracion()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para ver datos de configuración del sistema';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$config = json_decode( file_get_contents(URL::to('/configuracion.json')) );
			$msg['config']['factorCambio'] = $config->factorCambio;
			$msg['config']['porcentajeGastosAdministrativos'] = $config->porcentajeGastosAdministrativos;
			$msg['flag'] = 1;
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron cargar los datos de los usuarios\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postActualizarConfiguracion()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para actualizar datos de configuración del sistema';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$data = Input::all();
			$config = json_decode( file_get_contents(URL::to('/configuracion.json')) );
			$config->factorCambio = floatval($data['factorCambio']);
			$config->porcentajeGastosAdministrativos = floatval($data['porcentajeGastosAdministrativos']);
			$fileLocation = realpath(dirname(dirname(dirname(__FILE__))) . '/public/configuracion.json');
			$bytesWritten = File::put($fileLocation, json_encode($config));

			if ($bytesWritten === false) {
			    $msg['msg'] = 'Error tratando de abrir el archivo de configuración';
				$msg['flag'] = -1;
				return Response::json($msg);
			}

			$msg['msg'] = 'Configuración del sistema actualizada exitosamente';
			$msg['flag'] = 1;
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron cargar los datos de los usuarios\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postCargarTodos()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');

			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para ver datos de los usuarios';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$msg['users'] = Usuario::select('id', 'cedula', 'username', 'nombres', 'apellidos', 'tlf_movil', 'email')
										->where('id', '!=', Auth::id())
										->where('tipo_usuario', '!=', 'Super Administrador')
										->orderBy('cedula')
										->get();
			$msg['flag'] = 1;
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron cargar los datos de los usuarios\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postBuscar()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para buscar usuarios';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$busqueda = Input::all()[0];
			$users = Usuario::select('id', 'cedula', 'username', 'nombres', 'apellidos', 'tlf_movil', 'email')
								->whereRaw('id != '.Auth::id().' AND tipo_usuario != \'Super Administrador\'
											AND (cedula LIKE \'%'.$busqueda.'%\'
											OR username LIKE \'%'.$busqueda.'%\'
											OR nombres LIKE \'%'.$busqueda.'%\'
											OR apellidos LIKE \'%'.$busqueda.'%\'
											OR tlf_movil LIKE \'%'.$busqueda.'%\'
											OR email LIKE \'%'.$busqueda.'%\')')
								->orderBy('cedula')
								->get();
			if(count($users) === 0) {
				$msg['msg'] = 'No se pudieron encontrar resultados según lo indicado en la búsqueda';
				$msg['flag'] = -1;
			} else {
				$msg['users'] = $users;
				$msg['flag'] = 1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron encontrar resultados según lo indicado en la búsqueda\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postCargarUsuario()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para cargar los datos de otros usuarios';
				$msg['flag'] = -2;
				return Response::json($msg);
			}

			if($value = Input::get('id'))
				$id =  $value;
			else
				$id = Auth::id();

			$userData = Usuario::select('id', 'cedula', 'username', 'nombres', 'apellidos', 'fecha_nacimiento',
									 'direccion', 'tlf_movil', 'tlf_habitacion', 'email', 'email_alternativo')
									->where('id', '=', $id)
									->get();

			if($userData) {
				$msg['userData'] = $userData[0];
				$msg['userData']['tlf_movil'] = explode('-', $msg['userData']['tlf_movil']);
            	$msg['userData']['tlf_habitacion'] = explode('-', $msg['userData']['tlf_habitacion']);
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'No se pudieron encontrar los datos del usuario indicado';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron encontrar los datos del usuario indicado\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postCambiarClave()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para cambiar su clave';
				$msg['flag'] = -5;
				return Response::json($msg);
			}
			$data = Input::all();
			$password = Usuario::where('id', '=', Auth::id())->pluck('password');

			if(!Hash::check($data['old_password'], $password)) {
				$msg['msg'] = 'No coincide la contraseña actual';
				$msg['flag'] = -4;
				return Response::json($msg);
			}
			if($data['new_password'] !== $data['new_password2']) {
				$msg['msg'] = 'La contraseña nueva no coincide';
				$msg['flag'] = -3;
				return Response::json($msg);
			}
			if($data['old_password'] === $data['new_password']) {
				$msg['msg'] = 'La contraseña nueva es igual a la actual';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$result = Usuario::where('id', '=', Auth::id())->update( array('password' => Hash::make($data['new_password'])) );

			if($result) {
				$msg['msg'] = 'Cambio de contraseña exitoso';
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'Error en la base de datos, no se pudo cambiar su contraseña';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo cambiar su contraseña\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postRegistrar()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para agregar un usuario';
				$msg['flag'] = -3;
				return Response::json($msg);
			}
			$data = Input::all();

			if(Usuario::where('cedula', '=', $data['cedula'])->pluck('cedula')) {
				$msg['msg'] = 'La cédula indicada ya existe';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $username = '';
            $charLength = strlen($characters);
            $length = 6;
            mt_srand(time());

            for ($i = 0; $i < $length; $i++)
                $username .= $characters[mt_rand(0, $charLength - 1)];

			do {
				$data['username'] = $username . mt_rand(0, 9) . mt_rand(0, 9);
				$result = Usuario::where('username', '=', $data['username'])->pluck('username');
			} while($result != null);

			$mailData['username'] = $data['username'];
			$mailData['password'] = com_create_guid();
			$email = $data['email'];
			$name = $mailData['name'] = $data['nombres'] .' '. $data['apellidos'];
			$data['password'] = Hash::make($mailData['password']);
			$data['usuario_registro'] = $data['usuario_ua'] = Auth::id();
			$data['fecha_nacimiento'] = date('Y-m-d', strtotime($data['fecha_nacimiento']));
			$tlfMovil = '';
			$tlfHabitacion = '';

			foreach ($data['tlf_movil'] as $valor)
	            $tlfMovil .= $valor . '-';

	        foreach ($data['tlf_habitacion'] as $valor)
	            $tlfHabitacion .= $valor . '-';

			$data['tlf_movil'] = substr_replace($tlfMovil, '', strlen($tlfMovil) - 1);
	        $data['tlf_habitacion'] = substr_replace($tlfHabitacion, '', strlen($tlfHabitacion) - 1);

			if(Usuario::insert($data)) {

				$value = Mail::send('emails.user_account', $mailData, function($message) use ($email, $name)
				{
				    $message->to($email, $name)
				    		->subject('Bienvienido');
				});
				$msg['msg'] = 'Usuario agregado exitosamente\n';
				$msg['msg'] .= (empty($value)) ? 'Correo enviado al usuario con los datos de su cuenta'
										: 'No se pudo enviar el correo al usuario con sus datos de la cuenta';
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'Error en la base de datos, no se pudo agregar el usuario';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo agregar el usuario\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postActualizar()
	{
		try {
			$tipoUsuario = Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario');
			
			if(!Auth::check() || ($tipoUsuario !== 'Super Administrador' && $tipoUsuario !== 'Administrador')) {
				$msg['msg'] = 'No posee permisos para actualizar un usuario';
				$msg['flag'] = -4;
				return Response::json($msg);
			}
			$data = Input::all();
			$result = Usuario::select('id', 'cedula')
								->where('cedula', '=', $data['cedula'])
								->first();

			if(!isset($data['id']))
				$data['id'] = Auth::id();

			if(!empty($result['cedula']) && $result['id'] !== $data['id']) {
				$msg['msg'] = 'La cédula indicada ya existe';
				$msg['flag'] = -3;
				return Response::json($msg);
			}
			$result = Usuario::select('id', 'username')
								->where('username', '=', $data['username'])
								->first();

			if(!empty($result['username']) && $result['id'] !== $data['id']) {
				$msg['msg'] = 'El usuario indicado ya existe';
				$msg['flag'] = -2;
				return Response::json($msg);
			}

			$data['usuario_ua'] = Auth::id();
			$data['fecha_nacimiento'] = date('Y-m-d', strtotime($data['fecha_nacimiento']));
			$tlfMovil = '';
			$tlfHabitacion = '';

			foreach ($data['tlf_movil'] as $valor)
	            $tlfMovil .= $valor . '-';

	        foreach ($data['tlf_habitacion'] as $valor)
	            $tlfHabitacion .= $valor . '-';

			$data['tlf_movil'] = substr_replace($tlfMovil, '', strlen($tlfMovil) - 1);
	        $data['tlf_habitacion'] = substr_replace($tlfHabitacion, '', strlen($tlfHabitacion) - 1);

			if(Usuario::where('id', '=', $data['id'])->update($data)) {
				$msg['msg'] = 'Usuario actualizado exitosamente';
				$msg['flag'] = 1;

			} else {
				$flag = true;
				$result = Usuario::where('id', '=', $data['id'])->first();
				unset($result['password']);
				unset($result['tipo_usuario']);
				unset($result['usuario_registro']);
				unset($result['fecha_registro']);

				foreach ($data as $key => $value)
					if($result[0][$key] === $data[$key])
						$flag = false;

				if($flag)
					$msg['msg'] = 'El usuario ya está actualizado';
				else
					$msg['msg'] = 'Error en la base de datos, no se pudo actualizar el usuario';

				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo actualizar el usuario\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postEliminar()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para eliminar usuarios';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$userIds = Input::all();
			$idCount = count($userIds);
			$delete = true;

			foreach ($userIds as $id)
				if(!Usuario::where('id', '=', $id)->delete())
					$delete = false;
			

			if($delete) {
				$msg['msg'] = ($idCount === 1) ? 'Usuario eliminado exitosamente' : 'Usuarios indicados eliminados exitosamente';
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'Error con la base de datos, ';
		        $msg['msg'] .= ($idCount === 1) ? 'no se pudo eliminar el usuario'
		        					: 'no se pudieron eliminar alguno(s) o todos los usuarios indicados, por favor verifique';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudo eliminar ningún usuario\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}
}