<?php

class PedidoController extends BaseController {

	public function getIndex()
	{
		return View::make('pedidos.index');
	}

	public function getRegistrar()
	{
		return View::make('pedidos.modificar');
	}

	public function getModificar($id)
	{
		if (!preg_match('/^[0-9]+$/', $id))
			App::abort(404);

		return View::make('pedidos.modificar');
	}

	public function getImprimir($id)
	{
		if (!preg_match('/^[0-9]+$/', $id))
			App::abort(404);

		$factura['order'] = Pedido::select('id', 'id_cliente', 'descripcion', 'total')
									->where('id', '=', $id)
									->get()[0];
		$factura['client'] = Cliente::select('tipo_cliente', 'documento_identidad', 'nombre_cliente')
									->where('id', '=', $factura['order']->id_cliente)
									->get()[0];
		$factura['productList'] = CuerpoPedido::select('codigo_producto', 'cantidad', 'precio')
											->where('id_pedido', '=', $id)
											->get();
		foreach ($factura['productList'] as $index => $product) {
			$factura['productList'][$index]->descripcion = Producto::where('codigo', '=', $product['codigo_producto'])
																	->pluck('descripcion');
		}
		$html = View::make('reportes.factura')
						->withFactura($factura)
						->render();
		$pdf = App::make('MyMpdf');
		$pdf->newPDF(null, 'Letter', 12, 'Helvetica', 15, 15, 5, 5);
		$pdf->write($html);
		return $pdf->output('pedido-'.$id.'.pdf');
	}

	public function postCargarTodos()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para ver datos de los pedidos';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$msg['orders'] = Pedido::join('clientes', 'pedidos.id_cliente', '=', 'clientes.id')
									->select('pedidos.id', 'clientes.nombre_cliente', 'pedidos.total')
									->orderBy('pedidos.id')
									->get();
			$msg['flag'] = 1;
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron cargar los datos de los pedidos\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postBuscar()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para buscar pedidos';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$busqueda = Input::all()[0];
			$orders = Pedido::join('clientes', 'pedidos.id_cliente', '=', 'clientes.id')
								->select('pedidos.id', 'clientes.nombre_cliente', 'pedidos.total')
								->whereRaw('pedidos.id = \''.$busqueda.'\'
											OR clientes.nombre_cliente LIKE \'%'.$busqueda.'%\'
											OR pedidos.total = \''.$busqueda.'\'')
								->orderBy('pedidos.id')
								->get();

			if(count($orders) === 0) {
				$msg['msg'] = 'No se pudieron encontrar resultados según lo indicado en la búsqueda';
				$msg['flag'] = -1;
			} else {
				$msg['orders'] = $orders;
				$msg['flag'] = 1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron encontrar resultados según lo indicado en la búsqueda\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postCargarPedido()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para cargar los datos de un pedido';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$id = Input::get('id');
			$prices = [];
			$orderData = Pedido::join('clientes', 'pedidos.id_cliente', '=', 'clientes.id')
								->select('pedidos.id', 'clientes.nombre_cliente AS id_cliente', 'pedidos.descripcion', 'pedidos.total')
								->where('pedidos.id', '=', $id)
								->get()[0];
			$products = Pedido::join('cuerpo_pedido', 'pedidos.id', '=', 'cuerpo_pedido.id_pedido')
								->join('productos', 'cuerpo_pedido.codigo_producto', '=', 'productos.codigo')
								->select('cuerpo_pedido.codigo_producto', 'cuerpo_pedido.cantidad', 'cuerpo_pedido.precio',
										 'productos.precio1', 'productos.precio2')
								->where('pedidos.id', '=', Input::get('id'))
								->get();

			foreach($products as $product) {
				$price1 = $product['precio1'];
				$price2 = $product['precio2'];
				array_push($prices, array('precio1' => $price1, 'precio2' => $price2));
				$unitPrice = $product['precio'] / $product['cantidad'];

				if($unitPrice === $price1) {
					$product['priceType'] = 'precio1';
					$product['unitPrice'] = $price1;
					unset($product['precio1']);

				} else if($unitPrice === $price2) {
					$product['priceType'] = 'precio2';
					$product['unitPrice'] = $price2;
					unset($product['precio2']);
				}
			}
			$len = count($products);

			for($i = 0; $i < $len; $i++)
				if(isset($products[$i]['precio1']))
					unset($products[$i]['precio1']);
				else if(isset($products[$i]['precio2']))
					unset($products[$i]['precio2']);

			$orderData['listaProductos'] = $products;

			if($orderData) {
				$msg['orderData'] = $orderData;
				$msg['prices'] = $prices;
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'No se pudieron encontrar los datos del pedido indicado';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron encontrar los datos del pedido indicado\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postBuscarClientes()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para buscar clientes en un pedido';
				$msg['flag'] = -1;
				return Response::json($msg);
			}

			if(($term = Input::get('term')) === '   ')
				$term = '';

			$msg['results'] = Cliente::whereRaw('nombre_cliente LIKE \'%'.$term.'%\'')
										->orderBy('nombre_cliente')
										->lists('nombre_cliente');
			$msg['flag'] = 1;
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron cargar los clientes\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postBuscarProductos()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para buscar productos en un pedido';
				$msg['flag'] = -1;
				return Response::json($msg);
			}

			if(($term = Input::get('term')) === '   ')
				$term = '';

			$msg['results'] = Producto::whereRaw('codigo LIKE \'%'.$term.'%\'')
										->orderBy('codigo')
										->lists('codigo');
			$msg['flag'] = 1;
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron cargar los productos\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postBuscarPrecios()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para buscar precios en un pedido';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$msg['prices'] = Producto::select('precio1', 'precio2')
										->where('codigo', '=', Input::get('codigo'))
										->get()[0];
			$msg['flag'] = 1;
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudieron cargar los precios del producto indicado\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postRegistrar()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para agregar un pedido';
				$msg['flag'] = -5;
				return Response::json($msg);
			}
			$data = Input::all();

			if(!($id = Cliente::where('nombre_cliente', '=', $data['id_cliente'])->pluck('id')) ) {
				$msg['msg'] = 'El cliente indicado no está registrado\nDebe ingresarlo en el sistema primero\nPuede hacer esto en el módulo de clientes';
				$msg['flag'] = -4;
				return Response::json($msg);

			} else {
				$data['id_cliente'] = $id;
			}
			$productList = $data['listaProductos'];
			unset($data['listaProductos']);
			$productCodes = [];
			$updateProducts = [];

			foreach($productList as $product)
				array_push($productCodes, $product['codigo_producto']);

			if($savedProducts = Producto::select('codigo', 'cantidad', 'punto_reorden')->whereIn('codigo', $productCodes)->get()) {
				$notEnoughProducts = [];
				$flagOverloadInventory = false;

				foreach($productList as $orderProduct) {
					foreach($savedProducts as $storeProduct) {
						//Lista de Productos ordenados en el pedido
						$orderProductCode = $orderProduct['codigo_producto'];
						$orderProductCant = $orderProduct['cantidad'];
						//Lista de Productos existentes en el inventario
						$storeProductCode = $storeProduct['codigo'];
						$storeProductCant = $storeProduct['cantidad'];

						if($orderProductCode === $storeProductCode) {
							if($orderProductCant > $storeProductCant) {
								array_push($notEnoughProducts, $orderProductCode);
								$flagOverloadInventory = true;

							} else {
								$restante = $storeProductCant - $orderProductCant;

								if($restante <= $storeProduct['punto_reorden'])
									array_push($updateProducts, array('codigo' => $orderProductCode, 'cantidadRestante' => $restante, 'reordenar' => true));
								else
									array_push($updateProducts, array('codigo' => $orderProductCode, 'cantidadRestante' => $restante, 'reordenar' => false));
							}
							break;
						}
					}
				}

				if($flagOverloadInventory) {
					$msg['msg'] = 'Uno o varios productos del pedido superan la cantidad en inventario';
					$msg['notEnoughProducts'] = $notEnoughProducts;
					$msg['flag'] = -3;
					return Response::json($msg);
				}
			} else {
				$msg['msg'] = 'No ha indicado ningún producto en el pedido o ninguno de los productos indicados está registrado en el sistema';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$data['usuario_registro'] = $data['usuario_ua'] = Auth::id();

			if($id = Pedido::insertGetId($data)) {
				$len = count($productList);

				for($i = 0; $i < $len; $i++) {
					$productList[$i]['id_pedido'] = $id;
					unset($productList[$i]['priceType']);
					unset($productList[$i]['unitPrice']);
				}

				if(CuerpoPedido::insert($productList)) {
					$emailData['products'] = [];

					foreach($updateProducts as $product) {
						Producto::where('codigo', '=', $product['codigo'])
								->update(array('cantidad' => $product['cantidadRestante']));

						if($product['reordenar'])
							array_push($emailData['products'], array('codigo' => $product['codigo'], 'cantidad' => $product['cantidadRestante']));
					}

					if(!empty($emailData['products'])) {
						$user = Usuario::select('email', 'nombres', 'apellidos')
										->where('id', '=', Auth::id())
										->get()[0];
						$email = $user['email'];
						$name = $user['nombres'] . ' ' . $user['apellidos'];

						$value = Mail::send('emails.critical_inventory', $emailData, function($message) use ($email, $name)
						{
						    $message->to($email, $name)
						    		->subject('Productos con bajo inventario');
						});
					}
					$msg['msg'] = 'Pedido agregado exitosamente';
					$msg['flag'] = 1;
				}
			} else {
				$msg['msg'] = 'Error en la base de datos, no se pudo agregar el pedido';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo agregar el pedido\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postActualizar()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para actualizar un pedido';
				$msg['flag'] = -5;
				return Response::json($msg);
			}
			$data = Input::all();

			if(!($id = Cliente::where('nombre_cliente', '=', $data['id_cliente'])->pluck('id')) ) {
				$msg['msg'] = 'El cliente indicado no está registrado\nDebe ingresarlo en el sistema primero\nPuede hacer esto en el módulo de clientes';
				$msg['flag'] = -4;
				return Response::json($msg);

			} else {
				$data['id_cliente'] = $id;
			}
			$productList = $data['listaProductos'];
			unset($data['listaProductos']);
			$productCodes = [];
			$resetProducts = [];
			$updateProducts = [];
			$emailData['products'] = [];
			$oldProductList = CuerpoPedido::select('codigo_producto', 'cantidad')
											->where('id_pedido', '=', $data['id'])
											->get();

			foreach ($productList as $product)
				array_push($productCodes, $product['codigo_producto']);
			foreach ($oldProductList as $oldProduct)
				array_push($productCodes, $oldProduct['codigo_producto']);

			$productCodes = array_unique($productCodes);
			$savedProducts = Producto::select('codigo', 'cantidad', 'punto_reorden')
										->whereIn('codigo', $productCodes)
										->get();

			foreach ($oldProductList as $oldOrderProduct) {
				$flagStoreProduct = false;
				$productCode = $oldOrderProduct['codigo_producto'];

				foreach ($productList as $orderProduct) {

					if ($productCode === $orderProduct['codigo_producto']){
						$flagStoreProduct = true;
						break;
					}
				}

				if(!$flagStoreProduct) {
					foreach ($savedProducts as $storeProduct) {
						if($storeProduct['codigo'] === $productCode) {
							$criticalQuantity = $storeProduct['punto_reorden'];
							$actualQuantity = $storeProduct['cantidad'];
							$totalQuantity = $actualQuantity + $oldOrderProduct['cantidad'];
							Producto::where('codigo', '=', $productCode)
										->update(array('cantidad' => $totalQuantity));

							if(($actualQuantity <= $criticalQuantity) && ($totalQuantity > $criticalQuantity))
								array_push($emailData['products'], array('codigo' => $productCode, 'cantidadAnterior' => $actualQuantity, 'cantidadTotal' => $totalQuantity, 'puntoReorden' => $criticalQuantity));
							break;
						}
					}
				}
			}

			if (!empty($emailData['products'])) {
				$user = Usuario::select('email', 'nombres', 'apellidos')
								->where('id', '=', Auth::id())
								->get()[0];
				$email = $user['email'];
				$name = $user['nombres'] . ' ' . $user['apellidos'];
				$value = Mail::send('emails.restore_inventory', $emailData, function($message) use ($email, $name)
				{
				    $message->to($email, $name)
				    		->subject('Productos repuestos en el inventario');
				});
			}

			if($savedProducts) {
				$notEnoughProducts = [];
				$flagOverloadInventory = false;

				foreach($productList as $orderProduct) {
					foreach($savedProducts as $storeProduct) {
						//Lista de Productos ordenados en el pedido
						$orderProductCode = $orderProduct['codigo_producto'];
						$orderProductCant = $orderProduct['cantidad'];
						//Lista de Productos existentes en el inventario
						$storeProductCode = $storeProduct['codigo'];
						$storeProductCant = $storeProduct['cantidad'];

						if($orderProductCode === $storeProductCode) {
							$cantReset = CuerpoPedido::whereRaw('codigo_producto = \''.$storeProductCode.'\' AND id_pedido = '.$data['id'])
													->pluck('cantidad');

							if($cantReset)
								$storeCant = $storeProductCant + $cantReset;
							else
								$storeCant = $storeProductCant;
							
							if($orderProductCant > $storeCant) {
								array_push($notEnoughProducts, $orderProductCode);
								$flagOverloadInventory = true;

							} else {
								$restante = $storeCant - $orderProductCant;

								if($restante <= $storeProduct['punto_reorden'])
									array_push($updateProducts, array('codigo' => $orderProductCode, 'cantidadRestante' => $restante, 'reordenar' => true));
								else
									array_push($updateProducts, array('codigo' => $orderProductCode, 'cantidadRestante' => $restante, 'reordenar' => false));
							}
							break;
						}
					}
				}

				if($flagOverloadInventory) {
					$msg['msg'] = 'Uno o varios productos del pedido superan la cantidad en inventario';
					$msg['notEnoughProducts'] = $notEnoughProducts;
					$msg['flag'] = -3;
					return Response::json($msg);
				}
			} else {
				$msg['msg'] = 'No ha indicado ningún producto en el pedido o ninguno de los productos indicados está registrado en el sistema';
				$msg['flag'] = -2;
				return Response::json($msg);
			}
			$data['usuario_ua'] = Auth::id();

			if(Pedido::where('id', '=', $data['id'])->update($data)) {
				$len = count($productList);

				for($i = 0; $i < $len; $i++) {
					$productList[$i]['id_pedido'] = $data['id'];
					unset($productList[$i]['priceType']);
					unset($productList[$i]['unitPrice']);
				}

				if(!CuerpoPedido::where('id_pedido', '=', $data['id'])->delete()) {
					$msg['msg'] = 'Error en la base de datos, no se pudo actualizar el pedido';
					$msg['flag'] = -1;
					return Response::json($msg);
				}

				if(CuerpoPedido::insert($productList)) {
					$emailData['products'] = [];

					foreach($updateProducts as $product) {
						Producto::where('codigo', '=', $product['codigo'])
								->update(array('cantidad' => $product['cantidadRestante']));

						if($product['reordenar'])
							array_push($emailData['products'], array('codigo' => $product['codigo'], 'cantidad' => $product['cantidadRestante']));
					}

					if(!empty($emailData['products'])) {
						$user = Usuario::select('email', 'nombres', 'apellidos')
										->where('id', '=', Auth::id())
										->get()[0];
						$email = $user['email'];
						$name = $user['nombres'] . ' ' . $user['apellidos'];

						$value = Mail::send('emails.critical_inventory', $emailData, function($message) use ($email, $name)
						{
						    $message->to($email, $name)
						    		->subject('Productos con bajo inventario');
						});
					}
					$msg['msg'] = 'Pedido actualizado exitosamente';
					$msg['flag'] = 1;
				}
			} else {
				$flag = true;
				$result = Pedido::where('id', '=', $data['id'])->first();
				unset($result['usuario_registro']);
				unset($result['fecha_registro']);

				foreach ($data as $key => $value)
					if($result[0][$key] === $data[$key])
						$flag = false;

				if($flag)
					$msg['msg'] = 'El pedido ya está actualizado';
				else
					$msg['msg'] = 'Error en la base de datos, no se pudo actualizar el pedido';

				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
		    $msg['msg'] = 'Error en el servidor, no se pudo actualizar el pedido\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}

	public function postEliminar()
	{
		try {
			if(!Auth::check() || Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') !== 'Super Administrador') {
				$msg['msg'] = 'No posee permisos para eliminar pedidos';
				$msg['flag'] = -1;
				return Response::json($msg);
			}
			$orderIds = Input::all();
			$idCount = count($orderIds);
			$delete = true;

			foreach ($orderIds as $id)
				if(!Pedido::where('id', '=', $id)->delete())
					$delete = false;

			if($delete) {
				$msg['msg'] = ($idCount === 1) ? 'Pedido eliminado exitosamente' : 'Pedidos indicados eliminados exitosamente';
				$msg['flag'] = 1;

			} else {
				$msg['msg'] = 'Error con la base de datos, ';
		        $msg['msg'] .= ($idCount === 1) ? 'no se pudo eliminar el pedido'
		        					: 'no se pudieron eliminar alguno(s) o todos los pedidos indicados, por favor verifique';
				$msg['flag'] = -1;
			}
		}
		catch(Exception $e) {
			$msg['msg'] = 'Error en el servidor, no se pudo eliminar ningún pedido\nPara más información revise la consola de JavaScript de su navegador web';
		    $msg['error'] = $e;
			$msg['flag'] = 0;
		}
		return Response::json($msg);
	}
}