<?php

class TelefonoCliente extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'telefonos_clientes';
	public $timestamps = false;

	public function cliente()
	{
		return $this->belongsTo('Cliente', 'id_cliente');
	}
}