<?php

class Cliente extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'clientes';
	public $timestamps = false;

	public function telefonos()
	{
		return $this->hasMany('TelefonoCliente', 'id_cliente');
	}
}