<?php

class Pedido extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pedidos';
	public $timestamps = false;

	public function cuerpo()
	{
		return $this->hasMany('CuerpoPedido', 'id_pedido');
	}
}