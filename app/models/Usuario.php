<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'usuarios';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	public $timestamps = false;

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

    /**
	 * Get the token for the user session.
	 *
	 * @return mixed
	 */
	public function getRememberToken()
	{
		return null; //No se soporta esta opción en la aplicación
	}

	/**
	 * Set the token for the user session.
	 *
	 * @return string
	 */
	public function setRememberToken($token)
	{
		//No se soporta esta opción en la aplicación
	}

	/**
	 * Get the token name for the user session.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return null; //No se soporta esta opción en la aplicación
	}

	/*
	 * Método sobreescrito para ignorar el "remember token"
	 */
	public function setAttribute($key, $value)
	{
		$isRememberTokenAttribute = $key == $this->getRememberTokenName();
		if (!$isRememberTokenAttribute)
		{
		  parent::setAttribute($key, $value);
		}
	}
}
