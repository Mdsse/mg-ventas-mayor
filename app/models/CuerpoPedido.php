<?php

class CuerpoPedido extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cuerpo_pedido';
	public $timestamps = false;

	public function pedido()
	{
		return $this->belongsTo('Pedido', 'id_pedido');
	}
}