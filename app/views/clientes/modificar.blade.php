@extends('layouts.default')

@section('title')
	Módulo de Clientes
@stop

@section('description')
	Actualización de Datos de Cliente
@stop

@section('content')
	<section class="principal" ng-controller="ClientController as client">
		<form class="formulario" ng-submit="client.update()">
			<table>
				<tr>
					<td colspan="2">
						<h2>Datos Personales</h2>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="documento_identidad">Documento de Identidad: *</label>
						<br>
						<select ng-model="clientData.tipo_cliente" autofocus required>
							<option value=""></option>
							<option value="J">J</option>
							<option value="V">V</option>
							<option value="E">E</option>
						</select>
						<input type="text" ng-model="clientData.documento_identidad" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="nombre_cliente">Nombre del Cliente: *</label>
						<br>
						<input type="text" class="nombre" ng-model="clientData.nombre_cliente" required/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="estado_ubicacion">Estado: *</label>
						<br>
						<select ng-model="clientData.estado_ubicacion" ng-change="client.loadCities()" required>
							<option value=""></option>
	                        <option value="Amazonas">Amazonas</option>
	                        <option value="Anzoátegui">Anzoátegui</option>
	                        <option value="Apure">Apure</option>
	                        <option value="Aragua">Aragua</option>
	                        <option value="Barinas">Barinas</option>
	                        <option value="Bolívar">Bolívar</option>
	                        <option value="Carabobo">Carabobo</option>
	                        <option value="Cojedes">Cojedes</option>
	                        <option value="DeltaAmacuro">Delta Amacuro</option>
	                        <option value="DistritoCapital">Distrito Capital</option>
	                        <option value="Falcón">Falcón</option>
	                        <option value="Guárico">Guárico</option>
	                        <option value="Lara">Lara</option>
	                        <option value="Mérida">Mérida</option>
	                        <option value="Miranda">Miranda</option>
	                        <option value="Monagas">Monagas</option>
	                        <option value="NuevaEsparta">Nueva Esparta</option>
	                        <option value="Portuguesa">Portuguesa</option>
	                        <option value="Sucre">Sucre</option>
	                        <option value="Táchira">Táchira</option>
	                        <option value="Trujillo">Trujillo</option>
	                        <option value="Vargas">Vargas</option>
	                        <option value="Yaracuy">Yaracuy</option>
	                        <option value="Zulia">Zulia</option>
						</select>
					</td>
					<td>
						<label for="ciudad_ubicacion">Ciudad: *</label>
						<br>
						<select class="ciudad" ng-model="city" ng-include="cities" ng-change="client.changeCity()" required>
						</select>
						<br>
						<input type="text" ng-model="otherCity" ng-hide="hideOtherCity" ng-required="!hideOtherCity"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="direccion">Dirección: *</label>
						<br>
						<input type="text" class="direccion"  ng-model="clientData.direccion" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h2>Datos de Contacto</h2>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="tlf">Teléfono Móvil: *</label>
						<br>
						<div ng-repeat="phone in phones">
							<b><input type="text" class="tlf" ng-model="clientData.tlf[phone][0]" placeholder="04XX" pattern="^[0-9]{4}$" required> - <input type="text" class="tlf" ng-model="clientData.tlf[phone][1]" placeholder="123" pattern="^[0-9]{3}$" required> - <input type="text" class="tlf" ng-model="clientData.tlf[phone][2]" placeholder="4567" pattern="^[0-9]{4}$" required></b>
							<br>
						</div>
						<label class="clickable" title="Agrega un nuevo teléfono al final de los que ya están" ng-click="client.addPhone()"><i class="fa fa-plus fa-fw"></i>Agregar Teléfono</label>
						<label class="clickable" title="Elimina el último teléfono" ng-click="client.deletePhone()"><i class="fa fa-minus fa-fw"></i>Eliminar Teléfono</label>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="email">Email: *</label>
						<br>
						<input type="text" class="email" ng-model="clientData.email" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" value="Guardar" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="status" ng-hide="hideStatus" ng-bind-html="client.status"></div>
					</td>
				</tr>
			</table>
		</form>
	</section>
@stop