@extends('layouts.default')

@section('title')
	Módulo de Pedidos
@stop

@section('description')
	Búsqueda de Pedidos
@stop

@section('content')
	<section class="principal" ng-controller="OrderController as order">
		<a class="enlace-registro" href="{{ URL::action('PedidoController@getRegistrar') }}">
			<i class="fa fa-plus fa-fw"></i>Registrar pedido
		</a>
		<br>
		<section class="busqueda">
			<form ng-submit="order.search(true)">
				<input type="text" class="input-buscar" placeholder="Buscar Pedidos" autofocus ng-model="order.busqueda" ng-change="order.quickSearch()"/><i class="fa fa-search fa-fw icono-buscar" ng-click="order.search(true)"></i>
		        <br>
		        <input type="checkbox" class="buscar-instantaneo" ng-model="order.instant"/>
		        <label class="texto-checkbox">Búsqueda Instantánea</label>
		        <label class="borrar-varios" ng-hide="order.hideMultipleDelete()" ng-click="order.delete(order.rowsSelected)">
		            <i class="fa fa-trash-o fa-fw"></i>Eliminar Seleccionados
		        </label>
	        </form>
	        <br>
	        <br>
			<table class="resultados">
				<tr>
					<th class="columna-iconos"></th>
					<th>Pedido</th>
					<th>Cliente</th>
					<th>Total (Bs.)</th>
				</tr>
				<tr ng-repeat="pedido in allOrders">
					<td class="columna-iconos">
						<i class="fa fa-trash-o fa-2x icon borrar" title="Eliminar pedido" ng-click="order.delete(pedido.id)"></i><i class="fa fa-edit fa-2x icon editar" title="Modificar pedido" ng-click="order.redirectUpdate(pedido.id)"></i>
					</td>
					<td ng-class="{ rowActive: order.isRowSelected(pedido.id) }" ng-click="order.selectRow(pedido.id)" ng-bind="pedido.id"></td>
					<td ng-class="{ rowActive: order.isRowSelected(pedido.id) }" ng-click="order.selectRow(pedido.id)" ng-bind="pedido.nombre_cliente"></td>
					<td ng-class="{ rowActive: order.isRowSelected(pedido.id) }" ng-click="order.selectRow(pedido.id)" ng-bind="pedido.total | currency: ''"></td>
				</tr>
			</table>
		</section>
	</section>
@stop