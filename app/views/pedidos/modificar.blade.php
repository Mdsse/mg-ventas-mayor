@extends('layouts.default')

@section('title')
	Módulo de Pedidos
@stop

@section('description')
	@if (Route::currentRouteAction() === 'PedidoController@getRegistrar')
		Registro
	@elseif (Route::currentRouteAction() === 'PedidoController@getModificar')
		Actualización
	@endif
	de Datos de Pedido
@stop

@section('content')
	<section class="principal" ng-controller="OrderController as order">
		<form class="formulario" id="registro-pedido" ng-submit="@if (Route::currentRouteAction() === 'PedidoController@getRegistrar') order.insert() @elseif (Route::currentRouteAction() === 'PedidoController@getModificar') order.update() @endif">
			<table>
				<tr>
					<td colspan="2">
						<h2><span class="print" ng-click="order.print()"><i class="fa fa-print fa-fw" ></i>Imprimir</span></h2>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h2>Datos Generales</h2>
					</td>
				</tr>
				<tr>
					<td>
						<label for="cliente">Cliente: *<i class="fa fa-question-circle fa-fw ayuda" title="Escriba mínimo tres caracteres del nombre<br>de un cliente para realizar una búsqueda<br> en la base de datos y mostrar una lista<br>con los resultados"></i><i class="fa fa-search fa-fw" id="search-clients" title="Haga click para mostrar una<br>lista con todos los clientes"></i></label>
						<br>
						<input class="autocomplete-client" type="text" ng-model="orderData.id_cliente" autofocus required/>
					</td>
					<td>
						<label for="descripcion">Descripción:</label>
						<br>
						<input type="text" ng-model="orderData.descripcion"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div id="client-status"></div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h2>Lista de Productos</h2>
					</td>
				</tr>
				<tr>
	                <td colspan="2">
	                    <p>* Es obligatorio llenar al menos la primera fila de la tabla</p>
	                    <br>
	                    <table class="productos">
	                        <tr>
	                            <th>Código</th>
	                            <th>Cantidad</th>
	                            <th>Tipo de Precio</th>
	                            <th>Precio Unitario (Bs.)</th>
	                            <th>Precio (Bs.)</th>
	                        </tr>
	                        <tr ng-repeat="product in products">
	                            <td>
	                                <input type="text" class="product-input" ng-model="orderData.listaProductos[product].codigo_producto" ng-focus="order.selectProduct(product)" ng-class="{ autocompleteProduct: order.isProductInputSelected(product) }" required><i class="fa fa-search fa-fw search" title="Haga click para mostrar una lista con todos los productos" ng-mouseenter="order.addSearchClass()" ng-mouseleave="order.removeSearchClass()" ng-class="searchProductClass"></i>
	                                <div class="product-status"></div>
	                            </td>
	                            <td>
	                                <input type="text" class="quantity" ng-model="orderData.listaProductos[product].cantidad" ng-change="order.quantityChange(product)" required>
	                            </td>
	                            <td>
	                                <select class="price-type" ng-model="orderData.listaProductos[product].priceType" ng-change="order.priceTypeChange(product)" required>
										<option value=""></option>
										<option value="precio1">Precio 1</option>
										<option value="precio2">Precio 2</option>
									</select>
	                            </td>
	                            <td>
	                                <input currency-directive type="text" class="unit-price" ng-model="orderData.listaProductos[product].unitPrice" readonly="readonly">
	                                <div class="unit-price-status"></div>
	                            </td>
	                            <td>
	                                <input currency-directive type="text" class="price" ng-model="orderData.listaProductos[product].precio" readonly="readonly">
	                                <div class="price-status"></div>
	                            </td>
	                            <td class="columna-iconos">
	                            	<i class="fa fa-remove fa-2x icon" title="Eliminar producto" ng-click="order.deleteRow(product)"></i>
	                            </td>
	                        </tr>
	                        <tr>
	                        	<td colspan="5" class="total">
	                        		Total <label class="total-order" ng-bind="orderData.total | currency : 'Bs. '"></label>
	                        	</td>
	                        </tr>
	                        <tr>
	                        	<td class="last-row" colspan="5">
	                        		<label class="clickable" title="Agrega un nuevo producto al final de la tabla" ng-click="order.addProduct()"><i class="fa fa-plus fa-fw"></i>Agregar Producto</label>
									<label class="clickable" title="Elimina el último producto de la tabla" ng-click="order.deleteProduct()"><i class="fa fa-minus fa-fw"></i>Eliminar Producto</label>
	                        	</td>
	                        </tr>
	                    </table>
	                </td>
	            </tr>
				<tr>
					<td colspan="2">
						<input type="submit" value="@if (Route::currentRouteAction() === 'PedidoController@getRegistrar') Registrar @elseif (Route::currentRouteAction() === 'PedidoController@getModificar') Guardar @endif" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="status" ng-hide="hideStatus" ng-bind-html="order.status"></div>
					</td>
				</tr>
			</table>
		</form>
	</section>
@stop