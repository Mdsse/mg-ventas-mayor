<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<p>
			El sistema ha generado automáticamente este correo para indicarle que uno o varios productos, cuyo inventario se encontraba en un nivel crítico, fueron repuestos en una cierta cantidad proveniente de la cancelación de dichos productos en un pedido previamente registrado. Estos datos fueron calculados de acuerdo al último pedido modificado en el sistema.
		</p>
		<h3>Productos Repuestos en Inventario</h3>
		<table>
			<tr>
				<th>Código</th>
				<th>Punto de Reorden</th>
				<th>Cantidad Previa</th>
				<th>Cantidad Total Actual</th>
			</tr>
		@foreach($products as $product)
			<tr>
				<td>
					{{ $product['codigo'] }}
				</td>
				<td>
					{{ $product['puntoReorden'] }}
				</td>
				<td>
					{{ $product['cantidadAnterior'] }}
				</td>
				<td>
					{{ $product['cantidadTotal'] }}
				</td>
			</tr>
		@endforeach
		</table>
		<p>
			Es importante hacer un seguimiento de los productos anteriormente señalados, para mantener un inventario adecuado y en niveles óptimos, ya que, estos pueden estar próximos en agotarse.
			<br>
			<br>Saludos de parte de Electrónica M&G
		</p>
	</body>
</html>