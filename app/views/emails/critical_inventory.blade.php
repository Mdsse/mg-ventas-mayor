<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<p>
			El sistema ha generado automáticamente este correo para indicarle que uno o varios productos han alcanzado un bajo nivel de inventario, de acuerdo al valor mínimo recomendado para almacenar por producto. Estos datos fueron calculados de acuerdo al último pedido que fue procesado para un cliente mayorista.
		</p>
		<h3>Productos de Bajo Inventario</h3>
		<table>
			<tr>
				<th>Código</th>
				<th>Cantidad Almacenada</th>
			</tr>
		@foreach($products as $product)
			<tr>
				<td>
					{{ $product['codigo'] }}
				</td>
				<td>
					{{ $product['cantidad'] }}
				</td>
			</tr>
		@endforeach
		</table>
		<p>
			Se recomienda solicitar tan pronto sea posible, una orden de reabastecimiento de los productos anteriormente señalados, para mantener un inventario adecuado y en niveles óptimos.
			<br>
			<br>Saludos de parte de Electrónica M&G
		</p>
	</body>
</html>