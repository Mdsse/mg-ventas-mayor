<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
	</head>
	<body>
		<h2>Datos de su cuenta</h2>
		<p>
			Bienvenido <b>{{ $name }}</b>, el sistema automáticamente le ha asignado una identificación de usuario y una clave para que pueda iniciar sesión en el sistema de Ventas al Mayor de Electrónica M&G:
			<br>
			<br>
			<b>Usuario:</b> {{ $username }}
			<br>
			<b>Contraseña:</b> {{ $password }}
			<br>
			<br>
			A partir de este momento, si desea cambiar su <b>Usuario</b> y <b>Contraseña</b>, puede proceder haciendo uso de los datos personales suministrados, atendiendo a las siguientes instrucciones:
			<br>
			<br>
			&emsp;<b>Usuario:</b>
			<br>&emsp;&emsp;- Mínimo 4 caracteres y máximo 16 caracteres
			<br>&emsp;&emsp;- Puede contener las letras de la A a la Z en minúscula y/o mayúscula
			<br>&emsp;&emsp;- Puede contener números
			<br>&emsp;&emsp;- Puede contener cualquiera de los siguientes caracteres: _ -
			<br>
			<br>
			&emsp;<b>Contraseña:</b>
			<br>&emsp;&emsp;- Mínimo 6 caracteres y máximo 18 caracteres
			<br>&emsp;&emsp;- Puede contener las letras de la A a la Z en minúscula y/o mayúscula
			<br>&emsp;&emsp;- Puede contener números
			<br>&emsp;&emsp;- Puede contener cualquiera de los siguientes caracteres: * + / : . , $ % & # _ -
			<br>
			<br>Saludos de parte de Electrónica M&G
		</p>
	</body>
</html>