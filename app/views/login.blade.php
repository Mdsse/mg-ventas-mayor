@extends('layouts.login')

@section('content')
    <form id="login" autocomplete="off" ng-controller="FormController" ng-submit="login()">
        <div class="campo">
            <i class="fa fa-user fa-fw"></i>
            <input type="text" ng-model="datos.username" required/>
        </div>
        <div class="campo">
            <i class="fa fa-lock fa-fw"></i>
            <input type="password" ng-model="datos.password" required/>
        </div>
        <label></label>
        <input type="submit" id="iniciar-sesion" class="boton" value="Iniciar sesión"/>
        <div class="fix"></div>
    </form>
@stop