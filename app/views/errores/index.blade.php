<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Electrónica M&G | Ventas al Mayor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
    {{ HTML::style('css/app.css') }}
</head>

<body>
    <header class="menu-bar">
        <div class="contenido">
            <div class="logo">
                <img src="{{ URL::to('img/logo.png') }}" alt="">
            </div>
            <div class="fix"></div>
        </div>
    </header>
    <div class="pagina">
        <h1 class="titulo"> @yield('title')</h1>
        @yield('content')
    </div>
</body>

</html>