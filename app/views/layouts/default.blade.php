<!DOCTYPE html>
<html lang="es" ng-app="ventasMayor">

<head>
    <meta charset="UTF-8">
    <title>Electrónica M&G | Ventas al Mayor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
    {{ HTML::style('css/app.css') }}
    {{ HTML::style('css/vendor.css') }}
    @yield('css')
</head>

<body>
    <header class="menu-bar" ng-controller="MenuController as menu">
        <div class="contenido">
            <div class="logo">
                <img src="{{ URL::to('img/logo.png') }}" alt="">
            </div>
            <div class="min-logo">
                <img src="{{ URL::to('img/min-logo.png') }}" alt="">
            </div>
            <nav class="menu-usuario left" ng-mouseenter="menu.profile(true)"  ng-mouseleave="menu.profile(false)">
                <a class="usuario username" href="javascript:void(0)" @if (Auth::check()) title="{{Auth::user()->username}}" @endif>
                    <i class="fa fa-user fa-fw"></i>@if(Auth::check()){{Auth::user()->username}}@endif<i class="fa fa-caret-@{{ menu.arrow }} fa-fw arrow"></i>
                </a>
                <a class="usuario username" id="no-name" href="javascript:void(0)" @if (Auth::check()) title="{{Auth::user()->username}}" @endif>
                    <i class="fa fa-user fa-fw"></i><i class="fa fa-caret-@{{ menu.arrow }} fa-fw arrow"></i>
                </a>
                <nav class="opciones-usuario">
                    <ul>
                        @if (Auth::check() && Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') === 'Super Administrador')
                        <li>
                            <a href="{{ URL::to('/usuarios/configuracion') }}" ng-mouseenter="menu.spinConfigOption(true)" ng-mouseleave="menu.spinConfigOption(false)">
                                <i class="fa fa-cog fa-fw" ng-class="menu.configOption"></i>Configuración
                            </a>
                        </li>
                        @endif
                        <li>
                            <a href="{{ URL::to('/usuarios/perfil') }}">
                                <i class="fa fa-edit fa-fw"></i>Editar perfil
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/usuarios/cambiar-clave') }}">
                                <i class="fa fa-lock fa-fw"></i>Cambiar contraseña
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/logout') }}">
                                <i class="fa fa-sign-out fa-fw"></i>Cerrar sesión
                            </a>
                        </li>
                    </ul>
                </nav>
            </nav>
            <nav class="navegacion">
                <ul class="completo">
                    <li>
                        <a href="{{ URL::action('ProductoController@getIndex') }}">
                            <i class="fa fa-cubes fa-fw"></i>Productos
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::action('PedidoController@getIndex') }}">
                            <i class="fa fa-truck fa-fw"></i>Pedidos
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::action('ClienteController@getIndex') }}">
                            <i class="fa fa-male fa-fw"></i>Clientes
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::action('UsuarioController@getIndex') }}">
                            <i class="fa fa-users fa-fw"></i>Usuarios
                        </a>
                    </li>
                </ul>
                <nav ng-mouseenter="menu.items(true)" ng-mouseleave="menu.items(false)">
                    <ul class="min-bar">
                        <li>
                            <a href="javascript:void(0)">
                                <i class="fa fa-bars fa-fw descripcion" title="Menú"></i><i class="fa fa-caret-@{{ menu.barArrow }} fa-fw barArrow"></i>
                            </a>
                        </li>
                    </ul>
                    <nav class="min-menu">
                        <ul>
                            <li>
                                <a href="{{ URL::action('ProductoController@getIndex') }}">
                                    <i class="fa fa-cubes fa-fw"></i>Productos
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::action('PedidoController@getIndex') }}">
                                    <i class="fa fa-truck fa-fw"></i>Pedidos
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::action('ClienteController@getIndex') }}">
                                    <i class="fa fa-male fa-fw"></i>Clientes
                                </a>
                            </li>
                            <li>
                                <a href="{{ URL::action('UsuarioController@getIndex') }}">
                                    <i class="fa fa-users fa-fw"></i>Usuarios
                                </a>
                            </li>
                        </ul>
                    </nav>
                </nav>
            </nav>
            <nav class="menu-usuario" ng-mouseenter="menu.profile(true)" ng-mouseleave="menu.profile(false)">
                <a class="usuario username" href="javascript:void(0)" @if (Auth::check()) title="{{Auth::user()->username}}" @endif>
                    <i class="fa fa-user fa-fw"></i>@if(Auth::check()){{Auth::user()->username}}@endif<i class="fa fa-caret-@{{ menu.arrow }} fa-fw arrow"></i>
                </a>
                <nav class="opciones-usuario">
                    <ul>
                        @if (Auth::check() && Usuario::where('id', '=', Auth::id())->pluck('tipo_usuario') === 'Super Administrador')
                        <li>
                            <a href="{{ URL::to('/usuarios/configuracion') }}" ng-mouseenter="menu.spinConfigOption(true)" ng-mouseleave="menu.spinConfigOption(false)">
                                <i class="fa fa-cog fa-fw" ng-class="menu.configOption"></i>Configuración
                            </a>
                        </li>
                        @endif
                        <li>
                            <a href="{{ URL::to('/usuarios/perfil') }}">
                                <i class="fa fa-edit fa-fw"></i>Editar perfil
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/usuarios/cambiar-clave') }}">
                                <i class="fa fa-lock fa-fw"></i>Cambiar contraseña
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('/logout') }}">
                                <i class="fa fa-sign-out fa-fw"></i>Cerrar sesión
                            </a>
                        </li>
                    </ul>
                </nav>
            </nav>
            <div class="fix"></div>
        </div>
    </header>
    <div class="pagina">
        <h1 class="titulo"> @yield('title')
            <br>
            <small> @yield('description') </small>
        </h1>
        @yield('content')
    </div>
    <script>rootDir = "{{ URL::to('/') }}"</script>
    <!--jQuery está primero para que AngularJS pueda utilizarlo en vez de su jqLite-->
    {{ HTML::script('js/vendor.js') }}
    {{ HTML::script('js/app.js') }}
    @yield('javascript')
</body>

</html>