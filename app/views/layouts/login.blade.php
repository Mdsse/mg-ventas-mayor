<!DOCTYPE html>
<html lang="es" ng-app="login">

<head>
    <meta charset="UTF-8">
    <title>Electrónica M&G | Ventas al Mayor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
    {{ HTML::style('css/login.css') }}
    {{ HTML::style('css/vendor.css') }}
</head>

<body>
    <img src="{{ URL::to('img/body-bg.png') }}" alt="" class="fondo">
    <div class="pagina">
        <h1 class="titulo"><span>Electrónica M&G</span></h1>
        <h2 class="subtitulo">Sistema administrativo de ventas al mayor</h2>
        @yield('content')
    </div>
    <script>rootDir = "{{ URL::to('/') }}"</script>
    <!--jQuery está primero para que AngularJS pueda utilizarlo en vez de su jqLite-->
    {{ HTML::script('js/vendor.js') }}
    {{ HTML::script('js/app.js') }}
</body>

</html>