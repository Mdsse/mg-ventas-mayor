@extends('layouts.default')

@section('title')
	Módulo de Usuarios
@stop

@section('description')
	Búsqueda y Registro de Usuarios
@stop

@section('content')
	<section class="principal" ng-controller="UserController as user">
		<span ng-click="user.showForm()">
			<i class="fa fa-plus fa-fw"></i>Registrar Usuario<i class="fa fa-caret-@{{ user.arrow }} fa-fw"></i>
		</span>
		<form class="formulario" id="registro-usuario" ng-submit="user.insert()">
			<table>
				<tr>
					<td colspan="2">
						<h2>Datos Personales</h2>
					</td>
				</tr>
				<tr>
					<td>
						<label for="nombres">Nombres: *</label>
						<br>
						<input type="text" ng-model="userData.nombres" required/>
					</td>
					<td>
						<label for="apellidos">Apellidos: *</label>
						<br>
						<input type="text" ng-model="userData.apellidos" required/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="cedula">Cédula: *</label>
						<br>
						<input type="text" ng-model="userData.cedula" required/>
					</td>
					<td>
						<label for="fecha_nacimiento">Fecha de Nacimiento: *</label>
						<br>
						<input type="text" class="calendario" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="direccion">Dirección: *</label>
						<br>
						<input type="text" class="direccion"  ng-model="userData.direccion" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h2>Datos de Contacto</h2>
					</td>
				</tr>
				<tr>
					<td>
						<label for="tlf_movil">Teléfono Móvil: *</label>
						<br>
						<b><input type="text" class="tlf" ng-model="userData.tlf_movil.0" placeholder="04XX" pattern="^[0-9]{4}$" required> - <input type="text" class="tlf" ng-model="userData.tlf_movil.1" placeholder="123" pattern="^[0-9]{3}$" required> - <input type="text" class="tlf" ng-model="userData.tlf_movil.2" placeholder="4567" pattern="^[0-9]{4}$" required></b>
					</td>
					<td>
						<label for="tlf_habitacion">Teléfono de Habitación: *</label>
						<br>
						<b><input type="text" class="tlf" ng-model="userData.tlf_habitacion.0" placeholder="02XX" pattern="^[0-9]{4}$" required> - <input type="text" class="tlf_habitacion" ng-model="userData.tlf_habitacion.1" placeholder="1234567" pattern="^[0-9]{7}$" required></b>
					</td>
				</tr>
				<tr>
					<td>
						<label for="email">Email: *</label>
						<br>
						<input type="text" ng-model="userData.email" required/>
					</td>
					<td>
						<label for="email_alternativo">Email Alternativo:</label>
						<br>
						<input type="text" ng-model="userData.email_alternativo"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" value="Registrar" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="status" ng-hide="hideStatus" ng-bind-html="user.status"></div>
					</td>
				</tr>
			</table>
		</form>
		<br>
		<section class="busqueda">
			<form ng-submit="user.search(true)">
				<input type="text" class="input-buscar" placeholder="Buscar Usuarios" autofocus ng-model="user.busqueda" ng-change="user.quickSearch()"/><i class="fa fa-search fa-fw icono-buscar" ng-click="user.search(true)"></i>
		        <br>
		        <input type="checkbox" class="buscar-instantaneo" ng-model="user.instant"/>
		        <label class="texto-checkbox">Búsqueda Instantánea</label>
		        <label class="borrar-varios" ng-hide="user.hideMultipleDelete()" ng-click="user.delete(user.rowsSelected)">
		            <i class="fa fa-trash-o fa-fw"></i>Eliminar Seleccionados
		        </label>
	        </form>
	        <br>
	        <br>
			<table class="resultados">
				<tr>
					<th class="columna-iconos"></th>
					<th>Cédula</th>
					<th>Usuario</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Móvil</th>
					<th>Email</th>
				</tr>
				<tr ng-repeat="usuario in allUsers">
					<td class="columna-iconos">
						<i class="fa fa-trash-o fa-2x icon borrar" title="Eliminar usuario" ng-click="user.delete(usuario.id)"></i><i class="fa fa-edit fa-2x icon editar" title="Modificar usuario" ng-click="user.redirectUpdate(usuario.id)"></i>
					</td>
					<td ng-class="{ rowActive: user.isRowSelected(usuario.id) }" ng-click="user.selectRow(usuario.id)" ng-bind="usuario.cedula"></td>
					<td ng-class="{ rowActive: user.isRowSelected(usuario.id) }" ng-click="user.selectRow(usuario.id)" ng-bind="usuario.username"></td>
					<td ng-class="{ rowActive: user.isRowSelected(usuario.id) }" ng-click="user.selectRow(usuario.id)" ng-bind="usuario.nombres"></td>
					<td ng-class="{ rowActive: user.isRowSelected(usuario.id) }" ng-click="user.selectRow(usuario.id)" ng-bind="usuario.apellidos"></td>
					<td ng-class="{ rowActive: user.isRowSelected(usuario.id) }" ng-click="user.selectRow(usuario.id)" ng-bind="usuario.tlf_movil"></td>
					<td ng-class="{ rowActive: user.isRowSelected(usuario.id) }" ng-click="user.selectRow(usuario.id)" ng-bind="usuario.email"></td>
				</tr>
			</table>
		</section>
	</section>
@stop