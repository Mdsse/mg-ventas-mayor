@extends('layouts.default')

@section('title')
	Módulo de Usuarios
@stop

@section('description')
	Cambio de Contraseña
@stop

@section('content')
	<section class="principal" ng-controller="UserController as user">
		<form class="formulario" ng-submit="user.changePassword()">
			<table>
				<tr>
	                <td>
	                    <label for="old_password">Contraseña Actual:</label>
	                    <br>
	                    <input type="password" pattern="^[a-zA-Z0-9\*\+\/\:\.\,\$\%\&\#_-]{6,18}$" ng-model="userData.old_password" autofocus required>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    <label for="new_password">Contraseña Nueva: <i class="fa fa-question-circle fa-fw ayuda" title="<b>Puede estar formado por:</b><br>- Mínimo 6 caracteres<br>- Máximo 18 caracteres<br>- Letras mayúsculas y minúsculas<br>- Números<br>- Caracteres: * + / : . , $ % & # _ -"></i></label>
	                    <br>
	                    <input type="password" pattern="^[a-zA-Z0-9\*\+\/\:\.\,\$\%\&\#_-]{6,18}$" ng-model="userData.new_password"  required>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    <label for="new_password2">Confirmar Contraseña:</label>
	                    <br>
	                    <input type="password" pattern="^[a-zA-Z0-9\*\+\/\:\.\,\$\%\&\#_-]{6,18}$" ng-model="userData.new_password2" required>
	                </td>
	            </tr>
				<tr>
					<td>
						<input type="submit" value="Guardar" required/>
					</td>
				</tr>
				<tr>
					<td>
						<div class="status" ng-hide="hideStatus" ng-bind-html="user.status"></div>
					</td>
				</tr>
			</table>
		</form>
	</section>
@stop