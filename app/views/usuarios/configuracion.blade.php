@extends('layouts.default')

@section('title')
	Módulo de Usuarios
@stop

@section('description')
	Configuración del Sistema
@stop

@section('content')
	<section class="principal" ng-controller="UserController as user">
		<form class="formulario" ng-submit="user.configuration()">
			<table>
				<tr>
	                <td>
	                    <label for="factorCambio">Factor de Cambio de Moneda:</label>
	                    <br>
	                    <input type="text" ng-model="config.factorCambio" autofocus required>
	                </td>
	            </tr>
	            <tr>
	                <td>
	                    <label for="porcentajeGastosAdministrativos">Porcentaje de Gastos Administrativos:</label>
	                    <br>
	                    <input type="text" ng-model="config.porcentajeGastosAdministrativos" required>
	                </td>
	            </tr>
				<tr>
					<td>
						<input type="submit" value="Guardar" required/>
					</td>
				</tr>
				<tr>
					<td>
						<div class="status" ng-hide="hideStatus" ng-bind-html="user.status"></div>
					</td>
				</tr>
			</table>
		</form>
	</section>
@stop