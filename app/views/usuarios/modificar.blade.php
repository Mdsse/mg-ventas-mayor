@extends('layouts.default')

@section('title')
	Módulo de Usuarios
@stop

@section('description')
	Actualización de Datos de Usuario
@stop

@section('content')
	<section class="principal" ng-controller="UserController as user">
		<form class="formulario" ng-submit="user.update()">
			<table>
				<tr>
					<td colspan="2">
						<h2>Datos Personales</h2>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="username">Usuario: * <i class="fa fa-question-circle fa-fw ayuda" title="<b>Puede estar formado por:</b><br>- Mínimo 4 caracteres<br>- Máximo 16 caracteres<br>- Letras mayúsculas y minúsculas<br>- Números<br>- Caracteres: _ -"></i></label>
						<br>
						<input type="text" ng-model="userData.username" ng-readonly="user.readonly" autofocus/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="nombres">Nombres: *</label>
						<br>
						<input type="text" ng-model="userData.nombres" required/>
					</td>
					<td>
						<label for="apellidos">Apellidos: *</label>
						<br>
						<input type="text" ng-model="userData.apellidos" required/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="cedula">Cédula: *</label>
						<br>
						<input type="text" ng-model="userData.cedula" required/>
					</td>
					<td>
						<label for="fecha_nacimiento">Fecha de Nacimiento: *</label>
						<br>
						<input type="text" class="calendario" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="direccion">Dirección: *</label>
						<br>
						<input type="text" class="direccion"  ng-model="userData.direccion" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h2>Datos de Contacto</h2>
					</td>
				</tr>
				<tr>
					<td>
						<label for="tlf_movil">Teléfono Móvil: *</label>
						<br>
						<b><input type="text" class="tlf" ng-model="userData.tlf_movil.0" placeholder="04XX" pattern="^[0-9]{4}$" required> - <input type="text" class="tlf" ng-model="userData.tlf_movil.1" placeholder="123" pattern="^[0-9]{3}$" required> - <input type="text" class="tlf" ng-model="userData.tlf_movil.2" placeholder="4567" pattern="^[0-9]{4}$" required></b>
					</td>
					<td>
						<label for="tlf_habitacion">Teléfono de Habitación: *</label>
						<br>
						<b><input type="text" class="tlf" ng-model="userData.tlf_habitacion.0" placeholder="02XX" pattern="^[0-9]{4}$" required> - <input type="text" class="tlf_habitacion" ng-model="userData.tlf_habitacion.1" placeholder="1234567" pattern="^[0-9]{7}$" required></b>
					</td>
				</tr>
				<tr>
					<td>
						<label for="email">Email: *</label>
						<br>
						<input type="text" ng-model="userData.email" required/>
					</td>
					<td>
						<label for="email_alternativo">Email Alternativo:</label>
						<br>
						<input type="text" ng-model="userData.email_alternativo"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" value="Guardar" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="status" ng-hide="hideStatus" ng-bind-html="user.status"></div>
					</td>
				</tr>
			</table>
		</form>
	</section>
@stop