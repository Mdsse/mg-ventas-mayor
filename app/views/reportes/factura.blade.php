<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		{{ HTML::style('css/app.css') }}
	</head>
	<body>
		<div class="logo" align="center">
            <img src="{{ URL::to('img/logo.png') }}" alt=""/>
            <h2 align="center">Orden Número {{ $factura['order']->id }}</h2>
        </div>
        <table class="factura">
        	<tr>
        		<td>
        			<h3>Cliente:</h3>
        			<label>{{ $factura['client']->nombre_cliente }}</label>
        		</td>
        		<td>
        			<h3>RIF/Cédula:</h3>
        			<label>{{ $factura['client']->tipo_cliente }} - {{ $factura['client']->documento_identidad }}</label>
        		</td>
        	</tr>
        	<tr>
        		<td colspan="2">
        			<h3>Descripción:</h3>
        			<label>{{ $factura['order']->descripcion }}</label>
        			<br><br><br>
        		</td>
        	</tr>
        </table>
        <h2 align="center">Lista de Productos</h2>
        <table class="productos">
        	<tr>
        		<th>
        			Código
        		</th>
        		<th>
        			Descripción
        		</th>
        		<th>
        			Cantidad
        		</th>
        		<th>
        			Precio (Bs.)
        		</th>
        	</tr>
    	@foreach ($factura['productList'] as $product)
    		<tr>
    			<td>
    				{{ $product['codigo_producto'] }}
    			</td>
    			<td>
    				{{ $product['descripcion'] }}
    			</td>
    			<td>
    				{{ number_format($product['cantidad'], 0, '', ' ') }}
    			</td>
    			<td>
    				{{ number_format($product['precio'], 2, '.', ',') }}
    			</td>
    		</tr>
    	@endforeach
        	<tr>
            	<td colspan="4" class="total">
            		Total <label class="total-order"> Bs. {{ number_format($factura['order']->total, 2, '.', ',') }} </label>
            	</td>
            </tr>
        </table>
	</body>
</html>