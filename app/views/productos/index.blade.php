@extends('layouts.default')

@section('title')
	Módulo de Productos
@stop

@section('description')
	Búsqueda y Registro de Productos
@stop

@section('content')
	<section class="principal" ng-controller="ProductController as product">
		<span ng-click="product.showForm()">
			<i class="fa fa-plus fa-fw"></i>Registrar Producto<i class="fa fa-caret-@{{ product.arrow }} fa-fw"></i>
		</span>
		<form class="formulario" id="registro-producto" enctype="multipart/form-data" ng-submit="product.insert()">
			<table>
				<tr>
					<td colspan="2">
						<label for="imagen">Cargar Imagen:</label>
						<br>
						<img class="preview-img" alt=""/>
						<input type="file" class="img" name="imagen" accept="image/*"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h2>Datos Generales</h2>
					</td>
				</tr>
				<tr>
					<td>
						<label for="codigo">Código: *</label>
						<br>
						<input type="text" ng-model="productData.codigo" required/>
					</td>
					<td>
						<label for="descripcion">Descripción: *</label>
						<br>
						<input type="text" ng-model="productData.descripcion" required/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="cantidad">Cantidad: *</label>
						<br>
						<input type="text" ng-model="productData.cantidad" required/>
					</td>
					<td>
						<label for="bulto">Bulto:</label>
						<br>
						<input type="text" ng-model="productData.bulto"/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="punto_reorden">Inventario Mínimo: *</label>
						<br>
						<input type="text" ng-model="productData.punto_reorden" required/>
					</td>
					<td>
						<label for="costo">Costo: *</label>
						<br>
						<input type="text" ng-model="productData.costo" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="origen">Origen del Producto: *</label>
						<br>
						<select ng-model="productData.origen" required>
							<option value=""></option>
							<option value="Importado">Importado</option>
							<option value="Nacional">Nacional</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" value="Registrar" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="status" ng-hide="hideStatus" ng-bind-html="product.status"></div>
					</td>
				</tr>
			</table>
		</form>
		<br>
		<section class="busqueda">
			<form ng-submit="product.search(true)">
				<input type="text" class="input-buscar" placeholder="Buscar Productos" autofocus ng-model="product.busqueda" ng-change="product.quickSearch()"/><i class="fa fa-search fa-fw icono-buscar" ng-click="product.search(true)"></i>
		        <br>
		        <input type="checkbox" class="buscar-instantaneo" ng-model="product.instant"/>
		        <label class="texto-checkbox">Búsqueda Instantánea</label>
		        <label class="borrar-varios" ng-hide="product.hideMultipleDelete()" ng-click="product.delete(product.rowsSelected)">
		            <i class="fa fa-trash-o fa-fw"></i>Eliminar Seleccionados
		        </label>
	        </form>
	        <br>
	        <br>
			<table class="resultados">
				<tr>
					<th class="columna-iconos"></th>
					<th>Código</th>
					<th>Cantidad</th>
					<th>Precio 1 (Bs.)</th>
					<th>Precio 2 (Bs.)</th>
				</tr>
				<tr ng-repeat="producto in allProducts">
					<td class="columna-iconos">
						<i class="fa fa-trash-o fa-2x icon borrar" title="Eliminar producto" ng-click="product.delete(producto.id)"></i><i class="fa fa-edit fa-2x icon editar" title="Modificar producto" ng-click="product.redirectUpdate(producto.id)"></i>
					</td>
					<td ng-class="{ rowActive: product.isRowSelected(producto.id) }" ng-click="product.selectRow(producto.id)" ng-bind="producto.codigo"></td>
					<td ng-class="{ rowActive: product.isRowSelected(producto.id) }" ng-click="product.selectRow(producto.id)" ng-bind="producto.cantidad"></td>
					<td ng-class="{ rowActive: product.isRowSelected(producto.id) }" ng-click="product.selectRow(producto.id)" ng-bind="producto.precio1 | currency: ''"></td>
					<td ng-class="{ rowActive: product.isRowSelected(producto.id) }" ng-click="product.selectRow(producto.id)" ng-bind="producto.precio2 | currency: ''"></td>
				</tr>
			</table>
		</section>
	</section>
@stop