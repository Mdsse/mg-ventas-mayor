@extends('layouts.default')

@section('title')
	Módulo de Productos
@stop

@section('description')
	Actualización de Datos de Producto
@stop

@section('content')
	<section class="principal" ng-controller="ProductController as product">
		<form class="formulario" id="actualizar" enctype="multipart/form-data" ng-submit="product.update()">
			<table>
				<tr>
					<td colspan="2">
						<label for="imagen">Cargar Imagen:</label>
						<br>
						<img class="preview-img" ng-src="@{{image}}" alt=""/>
						<input type="file" class="img" name="imagen" accept="image/*"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<h2>Datos Generales</h2>
					</td>
				</tr>
				<tr>
					<td>
						<label for="codigo">Código: *</label>
						<br>
						<input type="text" ng-model="productData.codigo" autofocus required/>
					</td>
					<td>
						<label for="descripcion">Descripción: *</label>
						<br>
						<input type="text" ng-model="productData.descripcion" required/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="cantidad">Cantidad: *</label>
						<br>
						<input type="text" ng-model="productData.cantidad" required/>
					</td>
					<td>
						<label for="bulto">Bulto:</label>
						<br>
						<input type="text" ng-model="productData.bulto"/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="punto_reorden">Inventario Mínimo: *</label>
						<br>
						<input type="text" ng-model="productData.punto_reorden" required/>
					</td>
					<td>
						<label for="costo">Costo: *</label>
						<br>
						<input type="text" ng-model="productData.costo" required/>
					</td>
				</tr>
				<tr>
					<td>
						<label for="precio1">Precio 1: *</label>
						<br>
						<input type="text" readonly="readonly" ng-model="productData.precio1" required/>
					</td>
					<td>
						<label for="precio2">Precio 2: *</label>
						<br>
						<input type="text" readonly="readonly" ng-model="productData.precio2" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label for="origen">Origen del Producto: *</label>
						<br>
						<select ng-model="productData.origen" required>
							<option value=""></option>
							<option value="Importado">Importado</option>
							<option value="Nacional">Nacional</option>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" value="Guardar" required/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="status" ng-hide="hideStatus" ng-bind-html="product.status"></div>
					</td>
				</tr>
			</table>
		</form>
	</section>
@stop