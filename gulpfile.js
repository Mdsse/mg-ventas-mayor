var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.scripts([
        'bower_components/jquery/dist/jquery.js',
        'bower_components/angularjs/angular.js',
        'resources/js/jquery.tooltipster.min.js',
        'resources/js/jquery.datetimepicker.js',
        'resources/js/jquery-ui.min.js'
      ],
      'public/js/vendor.js',
      './')
    	.scripts([
    		'clientes.js',
        	'default.js',
        	'login.js',
        	'pedidos.js',
        	'productos.js',
        	'usuarios.js'
    	],
    	'public/js/app.js')
    	.styles([
    		'bower_components/font-awesome/css/font-awesome.min.css',
    		'resources/css/jquery-ui.min.css',
    		'resources/css/jquery.datetimepicker.css',
    		'resources/css/tooltipster.css'
    	],
    	'public/css/vendor.css',
    	'./')
    	.copy('bower_components/font-awesome/fonts', 'public/fonts', './');
});
