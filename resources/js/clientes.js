//AngularJS
(function() {
    var app = angular.module('clientApp', []);

    app.controller('ClientController', ['$scope', '$http', '$filter', '$sce', '$window',

        function($scope, $http, $filter, $sce, $window) {
            var url = $window.location.href;
            this.status = $sce.trustAsHtml('<i class="fa fa-spinner fa-spin fa-fw"></i>Guardando datos');
            this.arrow = 'down';
            this.busqueda = '';
            this.instant = false;
            this.rowsSelected = [];
            $scope.cities = '';
            $scope.city = this.city = '';
            $scope.otherCity = '';
            $scope.hideOtherCity = true;
            $scope.rowCount = 0;
            $scope.hideStatus = true;
            $scope.phones = [0];
            $scope.clientData = {};
            $scope.clientData.tlf = [
                []
            ];
            $scope.allClients = {};

            this.showForm = function() {
                angular.element('#registro-cliente').slideToggle();
                this.arrow = (this.arrow === 'down') ? 'up' : 'down';
            };

            $scope.loadCities = this.loadCities = function() {
                $scope.cities = rootDir + '/ciudades/' + $scope.clientData.estado_ubicacion + '.html';
            };

            this.changeCity = function(flag) {
                var selectedCity;

                if (flag) {
                    selectedCity = $scope.city = this.city;
                } else {
                    selectedCity = $scope.city = angular.element('.ciudad').val();
                }
                $scope.hideOtherCity = (selectedCity === '-OTRA-') ? false : true;
            };

            $scope.loadPhones = this.addPhone = function() {
                var len = $scope.phones.length;

                if (len < 4)
                    $scope.phones.push(len);
                else
                    alert('Ha llegado al límite de números telefónicos permitidos');
            };

            this.deletePhone = function() {
                var len = $scope.phones.length;

                if (len > 1) {
                    $scope.phones.pop();
                    $scope.clientData.tlf.splice(len - 1, 1);
                } else {
                    alert('No se puede eliminar el primer número telefónico');
                }
            };

            $scope.refresh = this.search = function(buscar) {
                var action = (typeof(buscar) === 'undefined') ? 'cargar-todos' : 'buscar';

                $http.post(rootDir + '/clientes/' + action, angular.toJson(this.busqueda)).
                success(function(data) {
                    var response = angular.fromJson(data);

                    switch (response.flag) {
                        case 0:
                            alert(response.msg.replace('\\n', '\n'));
                            console.log(response.error);
                            break;
                        case 1:
                            $scope.allClients = response.clients;
                            break;
                        default:
                            alert(response.msg.replace('\\n', '\n'));
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.loadClient = function(id) {
                $http.post(rootDir + '/clientes/cargar-cliente', {
                    'id': id
                }).
                success(function(data) {
                    var response = angular.fromJson(data);

                    switch (response.flag) {
                        case 0:
                            alert(response.msg.replace('\\n', '\n'));
                            console.log(response.error);
                            break;
                        case 1:
                            $scope.clientData = response.clientData;
                            var tlfs = $scope.clientData.tlf.length - 1;
                            $scope.loadCities();

                            $http.post($scope.cities, {
                                'responseType': 'text'
                            }).
                            success(function(data) {
                                var cities = data;
                                var selectedCity = '"' + $scope.clientData.ciudad_ubicacion + '"';

                                if (cities.match(selectedCity)) {
                                    $scope.city = $scope.clientData.ciudad_ubicacion;
                                } else {
                                    $scope.city = '-OTRA-';
                                    $scope.hideOtherCity = false;
                                    $scope.otherCity = $scope.clientData.ciudad_ubicacion;
                                }
                            }).
                            error(function(data) {
                                alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                                console.log(data);
                            });

                            for (var i = 0; i < tlfs; i++)
                                $scope.loadPhones();
                            break;
                        default:
                            alert(response.msg.replace('\\n', '\n'));
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.insert = function() {
                $scope.hideStatus = false;
                $scope.clientData.fecha_registro = $scope.clientData.fecha_ua = $filter('date')(new Date(), 'yyyy-MM-dd');
                $scope.clientData.ciudad_ubicacion = ($scope.hideOtherCity) ? $scope.city : $scope.otherCity;
                this.city = '';

                $http.post(rootDir + '/clientes/registrar', $scope.clientData).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace('\\n', '\n'));

                    if (response.flag === 1) {
                        $scope.refresh();
                        $scope.clientData = {};
                        $scope.loadCities();
                        $scope.city = $scope.otherCity = '';
                        $scope.hideOtherCity = true;
                        $scope.phones = [0];

                    } else if (!response.flag) {
                        console.log(response.error);
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.quickSearch = function() {
                if (this.instant)
                    this.search(true);
            };

            this.update = function() {
                $scope.hideStatus = false;
                $scope.clientData.fecha_ua = $filter('date')(new Date(), 'yyyy-MM-dd');
                $scope.clientData.ciudad_ubicacion = ($scope.hideOtherCity) ? $scope.city : $scope.otherCity;

                $http.post(rootDir + '/clientes/actualizar', $scope.clientData).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace('\\n', '\n'));

                    if (!response.flag)
                        console.log(response.error);
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.delete = function(ids) {
                var message = '¿Está seguro que desea eliminar ',
                    confirmed = confirm(message + ((typeof(ids) === 'number' && !isNaN(ids)) ? 'este cliente?' : 'los clientes seleccionados?'));

                if (confirmed) {
                    $http.post(rootDir + '/clientes/eliminar', angular.toJson(ids)).
                    success(function(data) {
                        $scope.rowCount = 0;
                        var response = angular.fromJson(data);
                        alert(response.msg.replace('\\n', '\n'));

                        if (response.flag === 1)
                            $scope.refresh();
                        else if (!response.flag)
                            console.log(response.error);
                    }).
                    error(function(data) {
                        alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                        console.log(data);
                    });
                }
            };

            this.selectRow = function(id) {
                var index;

                if ((index = this.rowsSelected.indexOf(id)) === -1) {
                    this.rowsSelected.push(id);
                    $scope.rowCount++;
                } else {
                    this.rowsSelected.splice(index, 1);
                    $scope.rowCount--;
                }
            };

            this.isRowSelected = function(id) {
                return this.rowsSelected.indexOf(id) !== -1;
            };

            this.hideMultipleDelete = function() {
                return $scope.rowCount < 2;
            };

            this.redirectUpdate = function(id) {
                $window.location.href = rootDir + '/clientes/modificar/' + id;
            };

            if (url === rootDir + '/clientes/index') {
                this.search();

            } else if (url.match(rootDir + '/clientes/modificar/[0-9]+')) {
                this.loadClient(url.substring(url.lastIndexOf('/') + 1));
            }
        }
    ]);
})();

//jQuery
$(document).ready(function() {

    //Controla el plugin "tooltipster" para colocar tooltips personalizados sobre tags de HTML en el módulo de usuarios
    $('.clickable').tooltipster({
        multiple: true,
        theme: 'tooltipster-theme'
    });
});
