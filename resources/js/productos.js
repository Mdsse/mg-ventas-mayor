//AngularJS
(function() {
    var app = angular.module('productApp', []);

    app.controller('ProductController', ['$scope', '$http', '$filter', '$sce', '$window',

        function($scope, $http, $filter, $sce, $window) {
            var url = $window.location.href;
            this.status = $sce.trustAsHtml('<i class="fa fa-spinner fa-spin fa-fw"></i>Guardando datos');
            this.arrow = 'down';
            this.busqueda = '';
            this.instant = false;
            this.rowsSelected = [];
            $scope.image = '';
            $scope.rowCount = 0;
            $scope.hideStatus = true;
            $scope.productData = {};
            $scope.allProducts = {};

            this.showForm = function() {
                angular.element('#registro-producto').slideToggle();
                this.arrow = (this.arrow === 'down') ? 'up' : 'down';
            };

            $scope.refresh = this.search = function(buscar) {
                var action = (typeof(buscar) === 'undefined') ? 'cargar-todos' : 'buscar';

                $http.post(rootDir + '/productos/' + action, angular.toJson(this.busqueda)).
                success(function(data) {
                    var response = angular.fromJson(data);

                    switch (response.flag) {
                        case 0:
                            alert(response.msg.replace('\\n', '\n'));
                            console.log(response.error);
                            break;
                        case 1:
                            $scope.allProducts = response.products;
                            break;
                        default:
                            alert(response.msg.replace('\\n', '\n'));
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.loadProduct = function(id) {
                $http.post(rootDir + '/productos/cargar-producto', {
                    'id': id
                }).
                success(function(data) {
                    var response = angular.fromJson(data);

                    switch (response.flag) {
                        case 0:
                            alert(response.msg.replace('\\n', '\n'));
                            console.log(response.error);
                            break;
                        case 1:
                            $scope.productData = response.productData;
                            var img = $scope.productData.imagen;

                            if (img !== null)
                                $scope.image += '/../mg-ventas-mayor/public/img/productos' + img.substring(img.lastIndexOf('/'));

                            break;
                        default:
                            alert(response.msg.replace('\\n', '\n'));
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.insert = function() {
                $scope.hideStatus = false;
                $scope.productData.fecha_registro = $scope.productData.fecha_ua = $filter('date')(new Date(), 'yyyy-MM-dd');

                $http.post(rootDir + '/productos/registrar', $scope.productData).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace('\\n', '\n'));

                    if (response.flag === 1) {
                        $scope.refresh();
                        $scope.productData = {};
                        saveImg(response.id, false);

                    } else if (!response.flag) {
                        console.log(response.error);
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.quickSearch = function() {
                if (this.instant)
                    this.search(true);
            };

            this.update = function() {
                $scope.hideStatus = false;
                $scope.productData.fecha_ua = $filter('date')(new Date(), 'yyyy-MM-dd');

                $http.post(rootDir + '/productos/actualizar', $scope.productData).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace('\\n', '\n'));

                    if (response.flag === 1) {
                        $scope.productData.precio1 = response.precio1;
                        $scope.productData.precio2 = response.precio2;
                        saveImg($scope.productData.id, true);

                    } else if (!response.flag) {
                        console.log(response.error);
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.delete = function(ids) {
                var message = '¿Está seguro que desea eliminar ',
                    confirmed = confirm(message + ((typeof(ids) === 'number' && !isNaN(ids)) ? 'este producto?' : 'los productos seleccionados?'));

                if (confirmed) {
                    $http.post(rootDir + '/productos/eliminar', angular.toJson(ids)).
                    success(function(data) {
                        $scope.rowCount = 0;
                        var response = angular.fromJson(data);
                        alert(response.msg.replace('\\n', '\n'));

                        if (response.flag === 1)
                            $scope.refresh();
                        else if (!response.flag)
                            console.log(response.error);
                    }).
                    error(function(data) {
                        alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                        console.log(data);
                    });
                }
            };

            this.selectRow = function(id) {
                var index;

                if ((index = this.rowsSelected.indexOf(id)) === -1) {
                    this.rowsSelected.push(id);
                    $scope.rowCount++;
                } else {
                    this.rowsSelected.splice(index, 1);
                    $scope.rowCount--;
                }
            };

            this.isRowSelected = function(id) {
                return this.rowsSelected.indexOf(id) !== -1;
            };

            this.hideMultipleDelete = function() {
                return $scope.rowCount < 2;
            };

            this.redirectUpdate = function(id) {
                $window.location.href = rootDir + '/productos/modificar/' + id;
            };

            if (url === rootDir + '/productos/index') {
                this.search();

            } else if (url.match(rootDir + '/productos/modificar/[0-9]+')) {
                this.loadProduct(url.substring(url.lastIndexOf('/') + 1));
            }
        }
    ]);
})();

//jQuery
$(document).ready(function() {
    var url = String(window.location),
        img = '',
        flag;

    if (flag = url.match(rootDir + '/productos/modificar/[0-9]+')) {
        $('.preview-img').css('display', 'block');
    }

    $('.img').change(function() {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            $('.preview-img').css('display', 'block');
            $('.preview-img').after().html('<br>');

            reader.onload = function(e) {
                var src = $('.preview-img').attr('src');

                if (flag && src !== '')
                    img = src;

                $('.preview-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        } else {
            if (flag && img !== '')
                $('.preview-img').attr('src', img);
            else
                $('.preview-img').css('display', 'none');
        }
    });

    saveImg = function(id, update) {
        var formData = (update) ? new FormData($('#actualizar')[0]) : new FormData($('#registro-producto')[0]);

        if (!update)
            $('.preview-img').css('display', 'none');

        $.ajax({
            url: rootDir + '/productos/guardar-imagen/' + id + '/' + update,
            type: 'POST',
            data: formData,
            error: function(e) {
                alert('Error intentado guardar imagen del producto');
                console.log(e);
            },
            success: function(data) {
                var response = data;

                if (response.flag !== 1) {
                    alert(response.msg);

                    if (!response.flag)
                        console.log(response.error);
                }
            },
            //Opciones para indicar a jQuery que no procese data o se preocupe por el tipo de contenido
            cache: false,
            contentType: false,
            processData: false
        });
    };
});
