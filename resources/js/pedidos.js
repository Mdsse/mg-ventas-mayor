//AngularJS
(function() {
    var app = angular.module('orderApp', []);

    /*app.directive('currencyDirective', ['$filter', function($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;

                ctrl.$formatters.unshift(function (a) {
                    return $filter('currency')(ctrl.$modelValue, '')
                });

                ctrl.$parsers.unshift(function (viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    elem.val($filter('currency')(plainNumber));
                    return plainNumber;
                });
            }
        }
    }])*/

    app.controller('OrderController', ['$scope', '$http', '$filter', '$sce', '$window',

        function($scope, $http, $filter, $sce, $window) {
            var url = $window.location.href;
            prices = [' '];
            total = 0;
            this.status = $sce.trustAsHtml('<i class="fa fa-spinner fa-spin fa-fw"></i>Guardando datos');
            this.busqueda = '';
            this.instant = false;
            this.rowsSelected = [];
            $scope.selectedProductInput = 0;
            $scope.searchProductClass = '';
            $scope.rowCount = 0;
            $scope.hideStatus = true;
            $scope.products = [0];
            $scope.orderData = {};
            $scope.orderData.total = 0;
            $scope.orderData.listaProductos = [];
            $scope.allOrders = {};

            updateTotal = function(value) {
                $scope.orderData.total = value;
                $scope.$digest();
            };

            this.addProduct = function() {
                var len = $scope.products.length;
                $scope.products.push(len);
                prices.push(' ');
            };

            this.deleteProduct = function() {
                var len = $scope.products.length;

                if (len > 1) {
                    //Devuelve en este caso un array de un solo elemento que contiene los datos del producto. El precio es la casilla 4
                    var price = $scope.orderData.listaProductos.splice(len - 1, 1)[0];

                    if (typeof(price) !== 'undefined')
                        price = price.precio;

                    prices.pop();
                    $scope.products.pop();

                    if (price !== '' && !isNaN(price))
                        $scope.orderData.total = total -= price;

                } else {
                    alert('No se puede eliminar el producto indicado\nEl pedido debe tener al menos un producto');
                }
            };

            $scope.refresh = this.search = function(buscar) {
                var action = (typeof(buscar) === 'undefined') ? 'cargar-todos' : 'buscar';

                $http.post(rootDir + '/pedidos/' + action, angular.toJson(this.busqueda)).
                success(function(data) {
                    var response = angular.fromJson(data);

                    switch (response.flag) {
                        case 0:
                            alert(response.msg.replace(/\\n/g, '\n'));
                            console.log(response.error);
                            break;
                        case 1:
                            $scope.allOrders = response.orders;
                            break;
                        default:
                            alert(response.msg.replace(/\\n/g, '\n'));
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.loadOrder = function(id) {
                $http.post(rootDir + '/pedidos/cargar-pedido', {
                    'id': id
                }).
                success(function(data) {
                    var response = angular.fromJson(data);

                    switch (response.flag) {
                        case 0:
                            alert(response.msg.replace(/\\n/g, '\n'));
                            console.log(response.error);
                            break;
                        case 1:
                            var data, len;
                            $scope.orderData = data = response.orderData;
                            $scope.products = [];
                            len = data.listaProductos.length;
                            prices = response.prices;
                            total = data.total;

                            for (var i = 0; i < len; i++)
                                $scope.products.push(i);
                            break;
                        default:
                            alert(response.msg.replace(/\\n/g, '\n'));
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.insert = function() {
                $scope.hideStatus = false;
                $scope.orderData.fecha_registro = $scope.orderData.fecha_ua = $filter('date')(new Date(), 'yyyy-MM-dd');

                $http.post(rootDir + '/pedidos/registrar', $scope.orderData).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace(/\\n/g, '\n'));

                    switch (response.flag) {
                        case -3:
                            var products = response.notEnoughProducts;

                            angular.element('.productos tr').each(function() {
                                var flag = false,
                                    row = angular.element(this),
                                    rowColumn = row.find('td').not('.columna-iconos'),
                                    rowColor = rowColumn.css('background-color');

                                for (var i in products)
                                    if (row.find('.product-input').val() === products[i])
                                        flag = true;

                                if (flag) {
                                    if (rowColor === 'rgba(0, 0, 0, 0)')
                                        rowColumn.css('background-color', 'rgba(231, 76, 60, 0.4)');

                                } else if (rowColor === 'rgba(231, 76, 60, 0.4)') {
                                    rowColumn.css('background-color', 'rgba(0, 0, 0, 0)');
                                }
                            });
                            break;
                        case 0:
                            console.log(response.error);
                            break;
                        case 1:
                            $scope.selectedProductInput = 0;
                            $scope.searchProductClass = '';
                            $scope.products = [0];
                            $scope.orderData = {};
                            $scope.orderData.total = 0;
                            prices = [' '];
                            total = 0;
                            row = angular.element('.productos tr').eq(1).find('td').not('.columna-iconos');

                            if (row.css('background-color') === 'rgba(231, 76, 60, 0.4)')
                                row.css('background-color', 'rgba(0, 0, 0, 0)');
                            break;
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.quickSearch = function() {
                if (this.instant)
                    this.search(true);
            };

            this.update = function() {
                $scope.hideStatus = false;
                $scope.orderData.fecha_ua = $filter('date')(new Date(), 'yyyy-MM-dd');

                $http.post(rootDir + '/pedidos/actualizar', $scope.orderData).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace(/\\n/g, '\n'));

                    switch (response.flag) {
                        case -3:
                            var products = response.notEnoughProducts;

                            angular.element('.productos tr').each(function() {
                                var flag = false,
                                    row = angular.element(this),
                                    rowColumn = row.find('td').not('.columna-iconos'),
                                    rowColor = rowColumn.css('background-color');

                                for (var i in products)
                                    if (row.find('.product-input').val() === products[i])
                                        flag = true;

                                if (flag) {
                                    if (rowColor === 'rgba(0, 0, 0, 0)')
                                        rowColumn.css('background-color', 'rgba(231, 76, 60, 0.4)');

                                } else if (rowColor === 'rgba(231, 76, 60, 0.4)') {
                                    rowColumn.css('background-color', 'rgba(0, 0, 0, 0)');
                                }
                            });
                            break;
                        case 0:
                            console.log(response.error);
                            break;
                        case 1:
                            row = angular.element('.productos tr').eq(1).find('td').not('.columna-iconos');

                            if (row.css('background-color') === 'rgba(231, 76, 60, 0.4)')
                                row.css('background-color', 'rgba(0, 0, 0, 0)');
                            break;
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.delete = function(ids) {
                var message = '¿Está seguro que desea eliminar ',
                    confirmed = confirm(message + ((typeof(ids) === 'number' && !isNaN(ids)) ? 'este pedido?' : 'los pedidos seleccionados?'));

                if (confirmed) {
                    $http.post(rootDir + '/pedidos/eliminar', angular.toJson(ids)).
                    success(function(data) {
                        $scope.rowCount = 0;
                        var response = angular.fromJson(data);
                        alert(response.msg.replace(/\\n/g, '\n'));

                        if (response.flag === 1)
                            $scope.refresh();
                        else if (!response.flag)
                            console.log(response.error);
                    }).
                    error(function(data) {
                        alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                        console.log(data);
                    });
                }
            };

            this.selectProduct = function(id) {
                $scope.selectedProductInput = id;
            };

            this.isProductInputSelected = function(id) {
                return $scope.selectedProductInput === id;
            };

            this.addSearchClass = function() {
                $scope.searchProductClass = 'searchProducts';
            };

            this.removeSearchClass = function() {
                $scope.searchProductClass = '';
            };

            this.quantityChange = function(i) {
                var product = $scope.orderData.listaProductos[i],
                    quantity = product.cantidad,
                    price = angular.element('.price').eq(i).val(),
                    unitPrice = angular.element('.unit-price').eq(i).val(),
                    regex = new RegExp('(^[0-9]{1,6}$)');

                if (price !== '' && !isNaN(price))
                    total -= price;

                if (regex.test(quantity) && product.priceType !== '' && unitPrice !== '' && !isNaN(unitPrice)) {
                    product.precio = quantity * unitPrice;
                    total += product.precio;

                } else {
                    product.precio = '';
                }
                $scope.orderData.listaProductos[i] = product;
                $scope.orderData.total = total;
            };

            this.priceTypeChange = function(i) {
                var product = $scope.orderData.listaProductos[i],
                    quantity = product.cantidad,
                    priceType = product.priceType,
                    price = angular.element('.price').eq(i).val(),
                    regex = new RegExp('(^[0-9]{1,6}$)');

                if (price !== '' && !isNaN(price))
                    total -= price;

                if (typeof(priceType) !== 'undefined' && prices[i] !== ' ') {
                    var price;

                    if (priceType === 'precio1')
                        price = prices[i].precio1;
                    else if (priceType === 'precio2')
                        price = prices[i].precio2;

                    product.unitPrice = price;

                    if (regex.test(quantity)) {
                        product.precio = quantity * price;
                        total += product.precio;

                    } else {
                        product.precio = '';
                    }

                } else {
                    product.unitPrice = '';
                    product.precio = '';
                }
                $scope.orderData.listaProductos[i] = product;
                $scope.orderData.total = total;
            };

            this.selectRow = function(id) {
                var index;

                if ((index = this.rowsSelected.indexOf(id)) === -1) {
                    this.rowsSelected.push(id);
                    $scope.rowCount++;
                } else {
                    this.rowsSelected.splice(index, 1);
                    $scope.rowCount--;
                }
            };

            this.isRowSelected = function(id) {
                return this.rowsSelected.indexOf(id) !== -1;
            };

            this.deleteRow = function(id) {
                var len = $scope.products.length,
                    price;

                if (len > 1) {
                    prices.splice(id, 1);
                    $scope.products.splice(id, 1);
                    //Devuelve en este caso un array de un solo elemento que contiene los datos del producto. El precio es la casilla 4
                    price = $scope.orderData.listaProductos.splice(id, 1)[0];
                    len--;

                    if (typeof(price) !== 'undefined')
                        price = price.precio;

                    if (price !== '' && !isNaN(price))
                        $scope.orderData.total = total -= price;

                    for (var i = 0; i < len; i++)
                        $scope.products[i] = i;

                } else {
                    alert('No se puede eliminar el producto indicado\nEl pedido debe tener al menos un producto');
                }
            };

            this.hideMultipleDelete = function() {
                return $scope.rowCount < 2;
            };

            this.redirectUpdate = function(id) {
                $window.location.href = rootDir + '/pedidos/modificar/' + id;
            };

            if (url === rootDir + '/pedidos/index') {
                this.search();

            } else if (url.match(rootDir + '/pedidos/modificar/[0-9]+')) {
                this.loadOrder(url.substring(url.lastIndexOf('/') + 1));
                this.readonly = true;

            }

            this.print = function() {
                $window.open(rootDir + '/pedidos/imprimir/' + $scope.orderData.id, '_blank');
            };
        }
    ]);
})();

//jQuery
$(document).ready(function() {

    //Controla el plugin "tooltipster" para colocar tooltips personalizados sobre tags de HTML en el módulo de usuarios
    $('.ayuda, #search-clients').tooltipster({
        contentAsHTML: true,
        multiple: true,
        theme: 'tooltipster-theme'
    });

    function ajaxAutocomplete(request, response, url, $statusSelector) {
        $.ajax({
            url: rootDir + url,
            type: 'POST',
            data: {
                term: request.term
            },
            beforeSend: function() {
                $statusSelector.html('<i class="fa fa-spinner fa-spin fa-fw"></i>   Buscando datos').show();
            },
            error: function() {
                $statusSelector.html('Error buscando la información').show();
            },
            success: function(data) {
                try {
                    $statusSelector.hide();

                    switch (data.flag) {
                        case 0:
                            alert(data.msg.replace(/\\n/g, '\n'));
                            console.log(data.error);
                            break;
                        case 1:
                            if (data.results == '')
                                $statusSelector.html('No se encontraron coincidencias').show();
                            else
                                response(data.results);
                            break;
                        default:
                            alert(data.msg.replace(/\\n/g, '\n'));
                    }
                } catch (e) {
                    alert('Error en la información recibida del servidor, no es válida. Esto indica un error en el servidor al buscar los datos');
                }
            }
        });
    }

    //Inicializa y controla la librería de interacción "jQuery UI" para colocar menús de autocomplete en inputs
    $('.autocomplete-client').autocomplete({
        autoFocus: true,
        minLength: 3,
        source: function(request, response) {
            ajaxAutocomplete(request, response, '/pedidos/buscar-clientes', $('#client-status'));
        }
    });
    $(document).on('keydown.autocomplete', '.autocompleteProduct', function() {
        var $productInput = $(this);

        $productInput.autocomplete({
            autoFocus: true,
            minLength: 3,
            source: function(request, response) {
                ajaxAutocomplete(request, response, '/pedidos/buscar-productos', $productInput.siblings('.product-status'));
            }
        });
    });

    $('#search-clients').click(function() {
        $('.autocomplete-client').autocomplete('search', '   ');
    });
    $(document).on('click', '.searchProducts', function() {
        var $search = $(this),
            $productInput = $search.siblings('.product-input');

        $productInput.autocomplete({
            autoFocus: true,
            minLength: 3,
            source: function(request, response) {
                ajaxAutocomplete(request, response, '/pedidos/buscar-productos', $productInput.siblings('.product-status'));
            }
        });
        $productInput.autocomplete('search', '   ');
    });

    $('.autocomplete-client').on('autocompleteclose', function(event, ui) {
        $(this).trigger('change');
    });
    $(document).on('autocompleteclose', '.autocompleteProduct', function(event, ui) {
        var flag = false,
            $productInput = $(this),
            $row = $(this).closest('tr'),
            priceType = $row.find('.price-type').val(),
            quantity = 0,
            $statusSelector;

        $('.autocompleteProduct').trigger('change');

        if (priceType !== '') {
            var status;
            flag = true;
            status = '.unit-price-status';
            cant = $row.find('.quantity').val();

            if (cant !== '' && !isNaN(cant) && cant !== '0') {
                status += ', .price-status';
                quantity = cant;
            }

            $statusSelector = $row.find(status);
        }

        $.ajax({
            url: rootDir + '/pedidos/buscar-precios',
            type: 'POST',
            data: {
                codigo: $productInput.val()
            },
            beforeSend: function() {
                if (flag)
                    $statusSelector.html('<i class="fa fa-spinner fa-spin fa-fw"></i>   Buscando datos').show();
            },
            error: function() {
                if (flag)
                    $statusSelector.html('Error buscando la información').show();
            },
            success: function(data) {
                try {
                    if (flag)
                        $statusSelector.hide();

                    switch (data.flag) {
                        case 0:
                            alert(data.msg.replace(/\\n/g, '\n'));
                            console.log(data.error);
                            break;
                        case 1:
                            var index = $row.index() - 1,
                                price = $row.find('.price').val();
                            prices[index] = [];
                            prices[index].precio1 = data.prices.precio1;
                            prices[index].precio2 = data.prices.precio2;

                            if (price !== '' && !isNaN(price))
                                total -= price;

                            if (flag) {
                                var price;

                                if (priceType === 'precio1')
                                    price = prices[index].precio1;
                                else if (priceType === 'precio2')
                                    price = prices[index].precio2;

                                $row.find('.unit-price').val(price);

                                if (quantity) {
                                    var value = price * parseInt(quantity);
                                    $row.find('.price').val(value);
                                    total += value;
                                    updateTotal(total);

                                } else {
                                    $row.find('.price').val('');
                                }

                            } else {
                                $row.find('.unit-price').val('');
                                $row.find('.price').val('');
                            }
                            break;
                        default:
                            alert(data.msg.replace(/\\n/g, '\n'));
                    }
                } catch (e) {
                    console.log(e);
                    alert('Error en la información recibida del servidor, no es válida. Esto indica un error en el servidor al buscar los datos');
                }
            }
        });
    });
});
