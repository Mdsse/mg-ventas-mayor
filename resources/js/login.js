//AngularJS
(function() {
    var app = angular.module('login', []);

    app.controller('FormController', ['$scope', '$http', '$window',
        function($scope, $http, $window) {
            $scope.datos = {
                'username': 'Usuario',
                'password': 'contraseña'
            };

            $scope.login = function() {
                $http.post(rootDir + '/login', $scope.datos).
                success(function(data) {
                    response = angular.fromJson(data);

                    if (response.flag)
                        $window.location.href = rootDir + '/productos/index';
                    else
                        alert(response.msg);

                }).
                error(function(data) {
                    console.log(data);
                    alert(data.error.message);
                });
            };
        }
    ]);
})();

//jQuery
$(document).ready(function() {
    var userModified = false,
        passwordModified = false;

    //Verifica al enfocar el input de texto y el de contraseña si no han sido modificados para limpiarlos
    $('input[type=text]').focusin(function() {
        if (!userModified) {
            $(this).val('');
        }
    });
    $('input[type=password]').focusin(function() {
        if (!passwordModified) {
            $(this).val('');
        }
    });

    //Verifica si el input de texto y el de contraseña han sido modificados o no para marcar los flags correspondientes
    $('input[type=text]').change(function() {
        userModified = true;
    });
    $('input[type=password]').change(function() {
        passwordModified = true;
    });
});
