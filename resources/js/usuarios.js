//AngularJS
(function() {
    var app = angular.module('userApp', []);

    app.controller('UserController', ['$scope', '$http', '$filter', '$sce', '$window',

        function($scope, $http, $filter, $sce, $window) {
            var url = $window.location.href;
            this.status = $sce.trustAsHtml('<i class="fa fa-spinner fa-spin fa-fw"></i>Guardando datos');
            this.arrow = 'down';
            this.busqueda = '';
            this.instant = false;
            this.readonly;
            this.rowsSelected = [];
            $scope.rowCount = 0;
            $scope.hideStatus = true;
            $scope.config = {};
            $scope.userData = {};
            $scope.allUsers = {};

            this.loadConfiguration = function() {
                $http.post(rootDir + '/usuarios/configuracion').
                success(function(data) {
                    var response = angular.fromJson(data);

                    if (response.flag === 1) {
                        $scope.config = response.config;

                    } else if (!response.flag) {
                        alert(response.msg.replace('\\n', '\n'));
                        console.log(response.error);
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.configuration = function() {
                $scope.hideStatus = false;

                $http.post(rootDir + '/usuarios/actualizar-configuracion', $scope.config).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace('\\n', '\n'));

                    if (!response.flag)
                        console.log(response.error);
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.showForm = function() {
                angular.element('#registro-usuario').slideToggle();
                this.arrow = (this.arrow === 'down') ? 'up' : 'down';
            };

            $scope.refresh = this.search = function(buscar) {
                var action = (typeof(buscar) === 'undefined') ? 'cargar-todos' : 'buscar';

                $http.post(rootDir + '/usuarios/' + action, angular.toJson(this.busqueda)).
                success(function(data) {
                    var response = angular.fromJson(data);

                    switch (response.flag) {
                        case 0:
                            alert(response.msg.replace('\\n', '\n'));
                            console.log(response.error);
                            break;
                        case 1:
                            $scope.allUsers = response.users;
                            break;
                        default:
                            alert(response.msg.replace('\\n', '\n'));
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.loadUser = function(id) {
                $http.post(rootDir + '/usuarios/cargar-usuario', {
                    'id': id
                }).
                success(function(data) {
                    var response = angular.fromJson(data);

                    switch (response.flag) {
                        case 0:
                            alert(response.msg.replace('\\n', '\n'));
                            console.log(response.error);
                            break;
                        case 1:
                            angular.element('.calendario').val($filter('date')(response.userData.fecha_nacimiento, 'dd/MM/yyyy'));
                            $scope.userData = response.userData;
                            break;
                        default:
                            alert(response.msg.replace('\\n', '\n'));
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.changePassword = function() {
                $scope.hideStatus = false;

                $http.post(rootDir + '/usuarios/cambiar-clave', $scope.userData).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace('\\n', '\n'));

                    if (response.flag === 1) {
                        $scope.userData = {};

                    } else if (!response.flag) {
                        console.log(response.error);
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.insert = function() {
                $scope.hideStatus = false;
                $scope.userData.fecha_nacimiento = angular.element('.calendario').val().replace(/\//g, '-');
                $scope.userData.fecha_registro = $scope.userData.fecha_ua = $filter('date')(new Date(), 'yyyy-MM-dd');

                $http.post(rootDir + '/usuarios/registrar', $scope.userData).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace('\\n', '\n'));

                    if (response.flag === 1) {
                        $scope.refresh();
                        $scope.userData = {};
                        angular.element('.calendario').val('');

                    } else if (!response.flag) {
                        console.log(response.error);
                    }
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.quickSearch = function() {
                if (this.instant)
                    this.search(true);
            };

            this.update = function() {
                $scope.hideStatus = false;
                $scope.userData.fecha_nacimiento = angular.element('.calendario').val().replace(/\//g, '-');
                $scope.userData.fecha_ua = $filter('date')(new Date(), 'yyyy-MM-dd');

                $http.post(rootDir + '/usuarios/actualizar', $scope.userData).
                success(function(data) {
                    $scope.hideStatus = true;
                    var response = angular.fromJson(data);
                    alert(response.msg.replace('\\n', '\n'));

                    if (!response.flag)
                        console.log(response.error);
                }).
                error(function(data) {
                    alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                    console.log(data);
                });
            };

            this.delete = function(ids) {
                var message = '¿Está seguro que desea eliminar ',
                    confirmed = confirm(message + ((typeof(ids) === 'number' && !isNaN(ids)) ? 'este usuario?' : 'los usuarios seleccionados?'));

                if (confirmed) {
                    $http.post(rootDir + '/usuarios/eliminar', angular.toJson(ids)).
                    success(function(data) {
                        $scope.rowCount = 0;
                        var response = angular.fromJson(data);
                        alert(response.msg.replace('\\n', '\n'));

                        if (response.flag === 1)
                            $scope.refresh();
                        else if (!response.flag)
                            console.log(response.error);
                    }).
                    error(function(data) {
                        alert('Error: ' + data.error.message + '\nPara más información revise la consola de JavaScript de su navegador web');
                        console.log(data);
                    });
                }
            };

            this.selectRow = function(id) {
                var index;

                if ((index = this.rowsSelected.indexOf(id)) === -1) {
                    this.rowsSelected.push(id);
                    $scope.rowCount++;
                } else {
                    this.rowsSelected.splice(index, 1);
                    $scope.rowCount--;
                }
            };

            this.isRowSelected = function(id) {
                return this.rowsSelected.indexOf(id) !== -1;
            };

            this.hideMultipleDelete = function() {
                return $scope.rowCount < 2;
            };

            this.redirectUpdate = function(id) {
                $window.location.href = rootDir + '/usuarios/modificar/' + id;
            };

            if (url === rootDir + '/usuarios/index') {
                this.search();

            } else if (url.match(rootDir + '/usuarios/modificar/[0-9]+')) {
                this.loadUser(url.substring(url.lastIndexOf('/') + 1));
                this.readonly = true;

            } else if (url === rootDir + '/usuarios/perfil') {
                this.loadUser();
                this.readonly = false;

            } else if (url === rootDir + '/usuarios/configuracion') {
                this.loadConfiguration();
            }
        }
    ]);
})();

//jQuery
$(document).ready(function() {
    var fechaActual = new Date();

    //Controla el plugin "tooltipster" para colocar tooltips personalizados sobre tags de HTML en el módulo de usuarios
    $('.ayuda').tooltipster({
        contentAsHTML: true,
        multiple: true,
        theme: 'tooltipster-theme'
    });

    //Maneja el plugin "datetimepicker" para mostrar un formato tipo calendario al momento de ingresar fechas en los formularios
    $('.calendario').datetimepicker({
        lang: 'es',
        timepicker: false,
        scrollInput: false,
        closeOnDateSelect: true,
        format: 'd/m/Y',
        formatDate: 'Y/m/d',
        minDate: '1920/01/01',
        maxDate: fechaActual.getFullYear() + '/' + fechaActual.getMonth + '/' + fechaActual.getDate(),
        yearStart: 1920,
        yearEnd: fechaActual.getFullYear()
    });
});
