//AngularJS
(function() {
    var app = angular.module('ventasMayor', ['userApp', 'clientApp', 'productApp', 'orderApp']);

    app.controller('MenuController', [

        function() {
            this.arrow = 'down';
            this.barArrow = 'down';
            this.configOption = '';

            this.profile = function(enter) {
                angular.element('.opciones-usuario').slideToggle();
                angular.element('.usuario').css('text-shadow', (enter) ? '1px 1px 8px #444' : 'none');
                this.arrow = (enter) ? 'up' : 'down';
            };

            this.items = function(enter) {
                angular.element('.min-menu').slideToggle();
                this.barArrow = (enter) ? 'up' : 'down';
            };

            this.spinConfigOption = function(enter) {
                this.configOption = (enter) ? 'fa-spin' : '';
            };
        }
    ]);
})();

//jQuery
$(document).ready(function() {

    //Controla el plugin "tooltipster" para colocar tooltips personalizados sobre tags de HTML en el menú del sistema
    $('.descripcion').tooltipster({
        multiple: true,
        theme: 'tooltipster-theme'
    });
    $('.username').tooltipster({
        multiple: true,
        offsetX: -12,
        offsetY: -35,
        position: 'bottom',
        theme: 'tooltipster-theme'
    });
});
